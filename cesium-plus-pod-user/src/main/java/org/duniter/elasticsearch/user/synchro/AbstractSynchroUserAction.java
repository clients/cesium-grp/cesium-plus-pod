package org.duniter.elasticsearch.user.synchro;

/*-
 * #%L
 * Cesium+ pod :: User plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.duniter.core.client.model.bma.EndpointApi;
import org.duniter.core.service.CryptoService;
import org.duniter.elasticsearch.user.PluginSettings;
import org.duniter.elasticsearch.client.Duniter4jClient;
import org.duniter.elasticsearch.synchro.AbstractSynchroAction;
import org.duniter.elasticsearch.threadpool.ThreadPool;
import org.duniter.elasticsearch.user.service.UserService;

public abstract class AbstractSynchroUserAction extends AbstractSynchroAction {

    private String endpointApi;

    public AbstractSynchroUserAction(String index, String type,
                                 Duniter4jClient client,
                                 PluginSettings pluginSettings,
                                 CryptoService cryptoService,
                                 ThreadPool threadPool) {
        super(index, type, index, type, client, pluginSettings.getDelegate(), cryptoService, threadPool);

        // Define endpoint API to used by synchronization, to select peers to request
        this.endpointApi = pluginSettings.getUserEndpointApi();

    }

    public AbstractSynchroUserAction(String fromIndex, String fromType,
                                 String toIndex, String toType,
                                 Duniter4jClient client,
                                 PluginSettings pluginSettings,
                                 CryptoService cryptoService,
                                 ThreadPool threadPool) {
        super(fromIndex, fromType, toIndex, toType, client, pluginSettings.getDelegate(), cryptoService, threadPool);

        // Define endpoint API to used by synchronization, to select peers to request
        this.endpointApi = pluginSettings.getUserEndpointApi();
    }

    @Override
    public String getEndPointApi() {
        return endpointApi;
    }

    /* -- internal methods -- */

    protected boolean hasUserProfile(final String pubkey) {
       return client.isDocumentExists(UserService.INDEX, UserService.PROFILE_TYPE, pubkey);
    }

    protected boolean hasUserSettings(final String pubkey) {
        return client.isDocumentExists(UserService.INDEX, UserService.SETTINGS_TYPE, pubkey);
    }

    protected boolean hasUserSettingsOrProfile(final String pubkey) {
        return hasUserSettings(pubkey) || hasUserProfile(pubkey);
    }

}
