package org.duniter.elasticsearch.user.execption;

/*-
 * #%L
 * Cesium+ pod :: User plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.duniter.elasticsearch.exception.NotFoundException;
import org.elasticsearch.rest.RestStatus;

public class UserProfileNotFoundException extends NotFoundException {
    public UserProfileNotFoundException(Throwable cause) {
        super(cause);
    }

    public UserProfileNotFoundException(String msg, Object... args) {
        super(msg, args);
    }

    public UserProfileNotFoundException(String msg, Throwable cause, Object... args) {
        super(msg, args, cause);
    }

    @Override
    public RestStatus status() {
        return RestStatus.FORBIDDEN;
    }
}
