package org.duniter.elasticsearch.user.synchro.group;

/*-
 * #%L
 * Duniter4j :: ElasticSearch User plugin
 * %%
 * Copyright (C) 2014 - 2017 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.duniter.elasticsearch.model.user.RecordComment;
import org.duniter.core.service.CryptoService;
import org.duniter.elasticsearch.client.Duniter4jClient;
import org.duniter.elasticsearch.synchro.SynchroAction;
import org.duniter.elasticsearch.synchro.SynchroActionResult;
import org.duniter.elasticsearch.synchro.SynchroService;
import org.duniter.elasticsearch.threadpool.ThreadPool;
import org.duniter.elasticsearch.user.PluginSettings;
import org.duniter.elasticsearch.user.dao.group.GroupCommentRepository;
import org.duniter.elasticsearch.user.dao.group.GroupIndexRepository;
import org.duniter.elasticsearch.user.dao.group.GroupRecordRepository;
import org.duniter.elasticsearch.user.execption.UserProfileNotFoundException;
import org.duniter.elasticsearch.user.synchro.AbstractSynchroUserAction;
import org.elasticsearch.common.inject.Inject;

public class SynchroGroupCommentAction extends AbstractSynchroUserAction {

    // Execute AFTER group record
    public static final int EXECUTION_ORDER = Math.max(
            SynchroAction.EXECUTION_ORDER_MIDDLE,
            SynchroGroupRecordAction.EXECUTION_ORDER + 1
    );

    @Inject
    public SynchroGroupCommentAction(Duniter4jClient client,
                                     PluginSettings pluginSettings,
                                     CryptoService cryptoService,
                                     ThreadPool threadPool,
                                     SynchroService synchroService) {
        super(GroupIndexRepository.INDEX, GroupCommentRepository.TYPE, client, pluginSettings, cryptoService, threadPool);

        setExecutionOrder(EXECUTION_ORDER);

        setEnableUpdate(true); // with update

        addValidationListener(this::onValidate);

        synchroService.register(this);
    }

    protected void onValidate(String id, JsonNode source, SynchroActionResult result) {

        String recordId = source.get(RecordComment.Fields.RECORD).asText();

        // Check issuer has a user profile
        if (client.isDocumentExists(GroupIndexRepository.INDEX, GroupRecordRepository.TYPE, recordId)) {
            throw new UserProfileNotFoundException(String.format("Comment on an unknown page {%s}.", recordId));
        }
    }
}
