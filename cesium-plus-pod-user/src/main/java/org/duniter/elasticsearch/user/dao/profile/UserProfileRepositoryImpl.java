package org.duniter.elasticsearch.user.dao.profile;

/*
 * #%L
 * Ğchange Pod :: ElasticSearch plugin
 * %%
 * Copyright (C) 2014 - 2017 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Lists;
import org.duniter.core.exception.TechnicalException;
import org.duniter.core.util.ObjectUtils;
import org.duniter.elasticsearch.dao.AbstractIndexTypeRepository;
import org.duniter.elasticsearch.exception.InvalidFormatException;
import org.duniter.elasticsearch.model.Record;
import org.duniter.elasticsearch.model.user.UserProfile;
import org.duniter.elasticsearch.user.PluginSettings;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortOrder;

import java.io.IOException;
import java.util.List;

/**
 * Created by blavenie on 03/04/17.
 */
public class UserProfileRepositoryImpl extends AbstractIndexTypeRepository<UserProfileRepositoryImpl> implements UserProfileRepository<UserProfileRepositoryImpl> {

    private PluginSettings pluginSettings;

    @Inject
    public UserProfileRepositoryImpl(PluginSettings pluginSettings) {
        super(UserIndexRepository.INDEX, UserProfileRepository.TYPE);
        this.pluginSettings = pluginSettings;
    }

    @Override
    protected void createIndex() throws JsonProcessingException {
        throw new TechnicalException("not implemented");
    }

    @Override
    public void checkSameDocumentIssuer(String id, String expectedIssuer) {
        String issuer = getMandatoryFieldsById(id, Record.Fields.ISSUER).get(Record.Fields.ISSUER).toString();
        if (!ObjectUtils.equals(expectedIssuer, issuer)) {
            throw new TechnicalException("Not same issuer");
        }
    }

    @Override
    public String create(final String json) {
        try {
            JsonNode actualObj = getObjectMapper().readTree(json);
            String issuer = actualObj.get(Record.Fields.ISSUER).asText();

            return create(issuer, json);
        }
        catch(IOException e) {
            throw new InvalidFormatException("Invalid record JSON: " + e.getMessage(), e);
        }
    }

    @Override
    public String create(final String issuer, final String json) {

        IndexResponse response = client.prepareIndex(getIndex(), getType())
                .setSource(json)
                .setId(issuer) // always use the issuer pubkey as id
                .setRefresh(false)
                .execute().actionGet();
        return response.getId();
    }

    @Override
    public Iterable<UserProfile> findAllGeoProfiles() {
        return findAllProfiles(QueryBuilders.existsQuery(UserProfile.Fields.GEO_POINT));
    }

    @Override
    public Iterable<UserProfile> findAllProfiles(QueryBuilder query) {
        if (logger.isDebugEnabled()) {
            logger.debug("Loading user profiles...");
        }

        long now = System.currentTimeMillis();
        int size = Math.max(1000, pluginSettings.getIndexBulkSize());

        QueryBuilder withGeoPoint = QueryBuilders.boolQuery()
            .filter(QueryBuilders.existsQuery(UserProfile.Fields.GEO_POINT));

        SearchRequestBuilder req = client.prepareSearch(UserProfileRepository.INDEX)
            .setTypes(UserProfileRepository.TYPE)
            .setScroll("1m")
            .setSize(size)
            .setFetchSource(true)
            .setQuery(QueryBuilders.constantScoreQuery(withGeoPoint))
            .addSort(UserProfile.Fields.TIME, SortOrder.ASC);

        List<UserProfile> result = Lists.newArrayList();
        long total = -1;
        int from = 0;
        String scrollId = null;
        do {
            SearchResponse response;
            if (scrollId == null) {
                response = client.safeExecuteRequest(req).actionGet();
                scrollId = response.getScrollId();
            }
            else {
                response = client.safeExecuteRequest(client.prepareSearchScroll(scrollId).setScroll("1m"))
                    .actionGet();
            }

            result.addAll(toList(response, UserProfile.class));

            from += size;
            req.setFrom(from);
            if (total == -1) total = response.getHits().getTotalHits();
        } while (from < total);

        // Clear scroll (async)
        if (scrollId != null) {
            client.prepareClearScroll().addScrollId(scrollId).execute();
        }

        if (logger.isDebugEnabled()) {
            if (result.size() > 0) {
                logger.debug("Loading user profiles [OK] {} profiles found in {} ms",
                    result.size(), System.currentTimeMillis() - now);
            }
            else {
                logger.debug("Loading user profiles [OK] no profiles found, in {} ms",
                    System.currentTimeMillis() - now);
            }
        }
        return result;
    }

    @Override
    public XContentBuilder createTypeMapping() {
        String stringAnalyzer = pluginSettings.getDefaultStringAnalyzer();

        try {
            XContentBuilder mapping = XContentFactory.jsonBuilder().startObject().startObject(getType())
                    .startObject("properties")

                    // version
                    .startObject(UserProfile.Fields.VERSION)
                    .field("type", "integer")
                    .endObject()

                    // title
                    .startObject(UserProfile.Fields.TITLE)
                    .field("type", "string")
                    .field("analyzer", stringAnalyzer)
                    .endObject()

                    // description
                    .startObject(UserProfile.Fields.DESCRIPTION)
                    .field("type", "string")
                    .field("analyzer", stringAnalyzer)
                    .endObject()

                    // time
                    .startObject(UserProfile.Fields.TIME)
                    .field("type", "integer")
                    .endObject()

                    // issuer
                    .startObject(UserProfile.Fields.ISSUER)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // city
                    .startObject(UserProfile.Fields.CITY)
                    .field("type", "string")
                    .endObject()

                    // address
                    .startObject(UserProfile.Fields.ADDRESS)
                    .field("type", "string")
                    .endObject()

                    // locale
                    .startObject(UserProfile.Fields.LOCALE)
                    .field("type", "string")
                    .endObject()

                    // geoPoint
                    .startObject(UserProfile.Fields.GEO_POINT)
                    .field("type", "geo_point")
                    .endObject()

                    // avatar
                    .startObject(UserProfile.Fields.AVATAR)
                    .field("type", "attachment")
                    .startObject("fields") // fields
                    .startObject("content") // content
                    .field("index", "no")
                    .endObject()
                    .startObject("title") // title
                    .field("type", "string")
                    .field("store", "no")
                    .endObject()
                    .startObject("author") // author
                    .field("store", "no")
                    .endObject()
                    .startObject("content_type") // content_type
                    .field("store", "yes")
                    .endObject()
                    .endObject()
                    .endObject()

                    // social networks
                    .startObject(UserProfile.Fields.SOCIALS)
                    .field("type", "nested")
                    .field("dynamic", "false")
                    .startObject("properties")
                    .startObject("type") // type
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()
                    .startObject("url") // url
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()
                    .endObject()
                    .endObject()

                    // tags
                    .startObject(UserProfile.Fields.TAGS)
                    .field("type", "completion")
                    .field("search_analyzer", "simple")
                    .field("analyzer", "simple")
                    .field("preserve_separators", "false")
                    .endObject()

                    .endObject()
                    .endObject().endObject();

            return mapping;
        }
        catch(IOException ioe) {
            throw new TechnicalException(String.format("Error while getting mapping for index [%s/%s]: %s", getIndex(), getType(), ioe.getMessage()), ioe);
        }
    }
}
