package org.duniter.elasticsearch.user.rest.user;

/*-
 * #%L
 * Cesium+ pod :: User plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableSortedSet;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.duniter.core.client.model.bma.jackson.JacksonUtils;
import org.duniter.core.client.model.local.Identity;
import org.duniter.core.client.model.local.Member;
import org.duniter.core.client.repositories.CurrencyRepository;
import org.duniter.core.exception.TechnicalException;
import org.duniter.core.util.ArrayUtils;
import org.duniter.core.util.Beans;
import org.duniter.core.util.CollectionUtils;
import org.duniter.core.util.DateUtils;
import org.duniter.elasticsearch.dao.MemberRepository;
import org.duniter.elasticsearch.exception.NotFoundException;
import org.duniter.elasticsearch.model.user.UserProfile;
import org.duniter.elasticsearch.rest.security.RestSecurityController;
import org.duniter.elasticsearch.threadpool.ThreadPool;
import org.duniter.elasticsearch.user.service.UserService;
import org.duniter.elasticsearch.utils.Geometries;
import org.elasticsearch.client.Client;
import org.elasticsearch.cluster.health.ClusterHealthStatus;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.logging.Loggers;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.rest.*;
import org.duniter.elasticsearch.user.PluginSettings;
import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.geojson.GeoJsonObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.elasticsearch.rest.RestRequest.Method.GET;

public class RestUserGeoJsonAction extends BaseRestHandler {

    private final ESLogger log;

    public static final String[] PROFILE_FIELDS = new String[]{
        UserProfile.Fields.ISSUER,
        UserProfile.Fields.TITLE,
        UserProfile.Fields.DESCRIPTION,
        UserProfile.Fields.ADDRESS,
        UserProfile.Fields.CITY
    };

    private final File file;

    private final CurrencyRepository currencyRepository;
    private final MemberRepository memberRepository;
    private final UserService userService;

    @Inject
    public RestUserGeoJsonAction(Settings settings, RestController controller, Client client,
                                 ThreadPool threadPool,
                                 PluginSettings pluginSettings,
                                 UserService userService,
                                 CurrencyRepository currencyRepository,
                                 MemberRepository memberRepository,
                                 RestSecurityController securityController) {
        super(settings, controller, client);
        log = Loggers.getLogger("duniter.rest.user.geojson", settings, String.format("[%s]", UserService.INDEX));
        String path =  String.format("/%s/%s/_geojson", UserService.INDEX, UserService.PROFILE_TYPE);
        controller.registerHandler(GET, path, this);
        securityController.allow(GET, path);
        this.userService = userService;
        this.currencyRepository = currencyRepository;
        this.memberRepository = memberRepository;
        file = new File(pluginSettings.getDelegate().getTempDirectory(),
            String.format("%s-%s.geojson", UserService.INDEX, UserService.PROFILE_TYPE));

        // Daily execution
        threadPool.scheduleAtFixedRate(
            this::generateProfilesGeoJsonFile,
            DateUtils.delayBeforeHour(pluginSettings.getUserMapExecuteHour()),
            DateUtils.DAY_DURATION_IN_MILLIS,
            TimeUnit.MILLISECONDS);

        threadPool.scheduleOnClusterHealthStatus(() -> {
            if (!file.exists() && DateUtils.delayBeforeHour(pluginSettings.getUserMapExecuteHour()) > 600000 /*10 min*/) {
                generateProfilesGeoJsonFile();
            }
        }, ClusterHealthStatus.GREEN);
    }

    @Override
    protected void handleRequest(RestRequest request, RestChannel channel, Client client) throws Exception {
        if (!file.exists()) {
            throw new NotFoundException("Map file not exists yet. Retry later");
        }

        try (InputStream is = new BufferedInputStream(FileUtils.openInputStream(file))) {
            final long fileLength = file.length();
            byte[] data = fileLength > 0 ? IOUtils.toByteArray(is, fileLength) : IOUtils.toByteArray(is);
            channel.sendResponse(new BytesRestResponse(RestStatus.OK, XContentType.JSON.restContentType(), data));
        }
    }

    protected synchronized void generateProfilesGeoJsonFile() {

        long now = System.currentTimeMillis();
        log.info("Generating user profiles map...");

        ObjectMapper objectMapper = JacksonUtils.getThreadObjectMapper();

        // Get members
        Map<String, Member> members = Beans.getStream((Iterable<String>) currencyRepository.findAllIds())
            .flatMap(currencyId -> memberRepository.getMembers(currencyId).stream())
            .collect(Collectors.toMap(Member::getPubkey, m -> m));

        // Get profiles with geo point
        Iterable<UserProfile> profiles = userService.findAllGeoProfiles();

        // Convert to GeoJson
        FeatureCollection features = toGeoJson(profiles, members, PROFILE_FIELDS);


        if (CollectionUtils.isNotEmpty(features.getFeatures())) {
            try {
                if (file.exists()) FileUtils.delete(file);

                // Write to file
                objectMapper.writeValue(file, features);

                log.info("Generating user profiles map [OK] {} profiles at {}, in {}ms",
                    features.getFeatures().size(), file.getAbsolutePath(), System.currentTimeMillis() - now);
            } catch (IOException e) {
                throw new TechnicalException("Cannot write user profile map file", e);
            }
        }
        else {
            if (file.exists()) {
                log.info("Generating user profiles map [OK] No profiles found. Removing file {}", file.getAbsolutePath());
                FileUtils.deleteQuietly(file);
            }
            else {
                log.info("Generating user profiles map [OK] No profiles found");
            }
        }
    }


    protected FeatureCollection toGeoJson(Iterable<UserProfile> profiles,
                                          Map<String, Member> members,
                                          String... fields) {
        FeatureCollection features = new FeatureCollection();

        Set<String> fieldList = ImmutableSortedSet.copyOf(fields);

        List<Feature> rows = Beans.getStream(profiles)
            .map(profile -> {
                Feature feature = new Feature();

                // Set properties
                Map<String, Object> properties = fieldList.stream()
                    .map(property -> {
                        try {
                            Object value = Beans.getProperty(profile, property);
                            if (value == null) return null; // Will be filtered
                            return new AbstractMap.SimpleEntry<>(property, value);
                        }
                        catch(Exception e) {
                            logger.warn("Cannot read property {} on profile {}", property, profile.getIssuer());
                            return null;
                        }
                    })
                    .filter(Objects::nonNull) // Avoid null value
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (o1,o2) -> o1, HashMap::new));

                // Set isMember or wasMember
                Member member = members.get(profile.getIssuer());
                boolean isMember = member != null && member.getIsMember() != null ? member.getIsMember() : false;
                boolean wasMember = member != null && member.getWasMember() != null ? member.getWasMember() : false;
                properties.put(Identity.Fields.UID, member != null ? member.getUid() : null);
                properties.put(Identity.Fields.IS_MEMBER, isMember);
                properties.put(Identity.Fields.WAS_MEMBER, wasMember);

                feature.setProperties(properties);

                // Set geometry
                GeoJsonObject geometry = Geometries.toGeometry(profile.getGeoPoint());
                feature.setGeometry(geometry);

                return feature;
            })
            .filter(feature -> feature.getGeometry() != null)
            .collect(Collectors.toList());

        features.setFeatures(rows);

        return features;
    }
}
