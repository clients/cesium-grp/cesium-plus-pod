package org.duniter.elasticsearch.model;

/*-
 * #%L
 * Cesium+ pod :: Core plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.duniter.core.client.model.local.Peer;
import org.duniter.core.client.service.HttpService;
import org.duniter.core.client.service.HttpServiceImpl;
import org.duniter.core.test.TestResource;
import org.duniter.elasticsearch.model.query.SearchScrollResponse;
import org.duniter.elasticsearch.service.ServiceLocator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

public class SearchScrollResponseTest {


    @ClassRule
    public static TestResource testResource = org.duniter.elasticsearch.TestResource.create();

    private HttpService service;

    @Before
    public void setUp() throws Exception {
        service = ServiceLocator.instance().getBean(HttpService.class);
        ((HttpServiceImpl)service).afterPropertiesSet();
    }

    @Test
    public void deserialize() {
        Peer peer = Peer.builder().host("g1-test.data.duniter.fr").port(443).useSsl(true).build();
        SearchScrollResponse response = service.executeRequest(peer, "/user/profile/_search?scroll=10s&size=1", SearchScrollResponse.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getScrollId());
    }
}
