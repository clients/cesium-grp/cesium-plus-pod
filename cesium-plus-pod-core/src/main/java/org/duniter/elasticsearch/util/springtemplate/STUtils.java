package org.duniter.elasticsearch.util.springtemplate;

/*-
 * #%L
 * Cesium+ pod :: Core plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupDir;

import java.util.Date;

public class STUtils {

    private STUtils() {
        /*help class*/
    }

    public static STGroup newSTGroup(String dirName) {
        // Configure springtemplate engine
        STGroup templates = new STGroupDir(dirName, '$', '$');
        templates.registerRenderer(Date.class, new DateRenderer());
        templates.registerRenderer(String.class, new StringRenderer());
        return templates;
    }


}
