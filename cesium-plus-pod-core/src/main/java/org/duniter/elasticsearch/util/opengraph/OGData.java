package org.duniter.elasticsearch.util.opengraph;

/*-
 * #%L
 * Cesium+ pod :: Core plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public class OGData {

    // min size for Facebook
    public static final int MIN_IMAGE_HEIGHT = 200;
    public static final int MIN_IMAGE_WIDTH = 200;

    public String type;
    public String title;
    public String description;
    public String image;
    public String url;
    public String locale;
    public String imageType;
    public String siteName;

    public Integer imageHeight;
    public Integer imageWidth;


    public String author;
    public String tag;

}
