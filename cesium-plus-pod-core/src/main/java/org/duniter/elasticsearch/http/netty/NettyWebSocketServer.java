package org.duniter.elasticsearch.http.netty;

/*-
 * #%L
 * Cesium+ pod :: Core plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.duniter.elasticsearch.http.netty.websocket.WebSocketEndpoint;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.logging.Loggers;
import org.elasticsearch.http.HttpServerTransport;

public class NettyWebSocketServer {

    private final ESLogger logger;
    private HttpServerTransport serverTransport;

    @Inject
    public  NettyWebSocketServer(HttpServerTransport serverTransport) {
        logger = Loggers.getLogger("duniter.ws");
        this.serverTransport = serverTransport;
    }

    public <T extends WebSocketEndpoint> void addEndpoint(String path, Class<T> handler) {
        if (serverTransport instanceof NettyHttpServerTransport) {
            NettyHttpServerTransport transport = (NettyHttpServerTransport)serverTransport;
            transport.addEndpoint(path, handler);
        }
        else {
            logger.warn("Ignoring websocket endpoint {" + handler.getName()+ "}: server transport is not compatible");
        }
    }

}
