package org.duniter.elasticsearch.http.netty.websocket;

/*-
 * #%L
 * Cesium+ pod :: Core plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.elasticsearch.common.bytes.BytesReference;
import org.jboss.netty.channel.SimpleChannelHandler;

import javax.websocket.CloseReason;

public class NettyBaseWebSocketEndpoint implements WebSocketEndpoint {

    @Override
    public void onOpen(NettyWebSocketSession session) {

    }

    @Override
    public void onMessage(String message) {

    }

    @Override
    public void onMessage(BytesReference bytes) {

    }

    public void onClose(CloseReason reason) {
    }

}
