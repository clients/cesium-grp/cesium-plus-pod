package org.duniter.elasticsearch.http.netty.websocket;

/*-
 * #%L
 * Cesium+ pod :: Core plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.elasticsearch.common.bytes.BytesReference;

import javax.websocket.CloseReason;

public interface WebSocketEndpoint {

    String WEBSOCKET_PATH = "/ws";

    void onOpen(NettyWebSocketSession session);

    void onMessage(String message);

    void onMessage(BytesReference bytes);

    void onClose(CloseReason reason);
}
