package org.duniter.elasticsearch.http.netty;

/*-
 * #%L
 * Cesium+ pod :: Core plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.duniter.elasticsearch.http.netty.websocket.NettyWebSocketSession;
import org.duniter.elasticsearch.http.netty.websocket.WebSocketEndpoint;
import org.elasticsearch.common.bytes.ChannelBufferBytesReference;
import org.elasticsearch.http.netty.NettyHttpRequest;
import org.jboss.netty.channel.*;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.websocketx.*;
import org.jboss.netty.logging.InternalLogger;
import org.jboss.netty.logging.InternalLoggerFactory;

import javax.websocket.CloseReason;
import java.nio.channels.ClosedChannelException;

@ChannelHandler.Sharable
public class WebSocketRequestHandler extends SimpleChannelHandler {

    private final WebSocketEndpoint endpoint;
    private NettyWebSocketSession session;

    private static final InternalLogger logger =
        InternalLoggerFactory.getInstance(WebSocketRequestHandler.class.getName());

    public WebSocketRequestHandler(WebSocketEndpoint endpoint) {
        super();
        this.endpoint = endpoint;
    }

    /* Do the handshaking for WebSocket request */
    public ChannelFuture handleHandshake(final NettyHttpRequest request) {
        WebSocketServerHandshakerFactory wsFactory =
                new WebSocketServerHandshakerFactory(getWebSocketURL(request), null, true);
        WebSocketServerHandshaker handshaker = wsFactory.newHandshaker(request.request());
        if (handshaker == null) {
            return wsFactory.sendUnsupportedWebSocketVersionResponse(request.getChannel());
        }

        ChannelFuture future = handshaker.handshake(request.getChannel(), request.request());
        future.addListener(channelFuture -> {
            // Session is open
            session = new NettyWebSocketSession(channelFuture.getChannel(), request.params());
            endpoint.onOpen(session);
        });
        return future;
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        if (endpoint == null) return; // not open

        Object msg = e.getMessage();
        if (msg instanceof NettyWebSocketSession) {
            endpoint.onOpen((NettyWebSocketSession)msg);
        }

        else if (msg instanceof WebSocketFrame) {

            // Received binary
            if (msg instanceof BinaryWebSocketFrame) {
                BinaryWebSocketFrame frame = (BinaryWebSocketFrame)msg;
                endpoint.onMessage(new ChannelBufferBytesReference(frame.getBinaryData()));
            }

            // Received text
            else if (msg instanceof TextWebSocketFrame) {
                TextWebSocketFrame frame = (TextWebSocketFrame) msg;
                endpoint.onMessage(frame.getText());
            }

            // Ping event
            else if (msg instanceof PingWebSocketFrame) {
               // TODO
            }

            // Pong event
            else if (msg instanceof PongWebSocketFrame) {
                // TODO
            }

            // Close
            else if (msg instanceof CloseWebSocketFrame) {
                ctx.getChannel().close();
                CloseWebSocketFrame frame = (CloseWebSocketFrame)msg;
                endpoint.onClose(new CloseReason(getCloseCode(frame), frame.getReasonText()));
            }

            // Unknown event
            else {
                System.out.println("Unsupported WebSocketFrame");
            }
        }
    }

    protected String getWebSocketURL(NettyHttpRequest req) {
        return "ws://" + req.request().headers().get(HttpHeaders.Names.HOST) + req.rawPath() ;
    }

    protected CloseReason.CloseCode getCloseCode(CloseWebSocketFrame frame) {

        int statusCode = frame.getStatusCode();
        if (statusCode == -1) return CloseReason.CloseCodes.NO_STATUS_CODE;
        try {
            return CloseReason.CloseCodes.getCloseCode(statusCode);
        }
        catch(IllegalArgumentException e) {
            return CloseReason.CloseCodes.NO_STATUS_CODE;
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        Throwable cause = e.getCause();
        if (this == ctx.getPipeline().getLast() && cause != null) {
            if (cause instanceof ClosedChannelException) {
                // Silent
            }
            else {
                logger.error("Unexpected websocket error: " + cause.getMessage(), e.getCause());
            }
        }
        ctx.sendUpstream(e);
    }
}
