package org.duniter.elasticsearch.http.netty.websocket;

/*-
 * #%L
 * Cesium+ pod :: Core plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.elasticsearch.common.bytes.BytesReference;
import org.elasticsearch.http.netty.NettyHttpRequest;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import org.jboss.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import org.jboss.netty.handler.codec.http.websocketx.TextWebSocketFrame;

import javax.websocket.CloseReason;
import java.io.IOException;
import java.util.Map;

public class NettyWebSocketSession {

    private Channel channel;
    private Map<String, String> pathParameters;

    public NettyWebSocketSession(Channel channel, Map<String, String> pathParameters) {
        this.channel = channel;
        this.pathParameters = pathParameters;
    }

    public void close(CloseReason closeReason) throws IOException {

        CloseWebSocketFrame frame = new CloseWebSocketFrame(closeReason.getCloseCode().getCode(), closeReason.getReasonPhrase());
        ChannelFuture future = channel.write(frame);

        future.addListener(ChannelFutureListener.CLOSE);
    }

    public void sendText(String text) {
        if (!channel.isOpen()) return; // Skip if channel is closed
        channel.write(new TextWebSocketFrame(text));
    }

    public void sendBinary(ChannelBuffer buffer) {
        if (!channel.isOpen()) return; // Skip if channel is closed
        BinaryWebSocketFrame frame = new BinaryWebSocketFrame();
        frame.setBinaryData(buffer);
        channel.write(frame);
    }

    public void sendBinary(BytesReference bytes) {
        sendBinary(bytes.toChannelBuffer());
    }

    public Map<String, String> getPathParameters() {
        return pathParameters;
    }

    public void setPathParameters( Map<String, String> pathParameters) {
        this.pathParameters = pathParameters;
    }

    public String getId() {
        return String.valueOf(this.hashCode());
    }

}
