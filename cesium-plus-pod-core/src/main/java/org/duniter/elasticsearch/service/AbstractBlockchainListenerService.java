package org.duniter.elasticsearch.service;

/*
 * #%L
 * Duniter4j :: Core API
 * %%
 * Copyright (C) 2014 - 2015 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableList;
import org.duniter.core.client.model.bma.BlockchainBlock;
import org.duniter.core.exception.TechnicalException;
import org.duniter.core.service.CryptoService;
import org.duniter.core.util.Preconditions;
import org.duniter.elasticsearch.PluginSettings;
import org.duniter.elasticsearch.client.Duniter4jClient;
import org.duniter.elasticsearch.dao.BlockRepository;
import org.duniter.elasticsearch.service.changes.ChangeEvent;
import org.duniter.elasticsearch.service.changes.ChangeService;
import org.duniter.elasticsearch.service.changes.ChangeSource;
import org.duniter.elasticsearch.threadpool.ThreadPool;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.common.bytes.BytesReference;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.unit.TimeValue;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Created by Benoit on 26/04/2017.
 */
public abstract class AbstractBlockchainListenerService extends AbstractService implements ChangeService.ChangeListener {

    private static final List<ChangeSource> CHANGE_LISTEN_SOURCES = ImmutableList.of(new ChangeSource("*", BlockRepository.TYPE));

    protected final String listenerId;
    protected final ThreadPool threadPool;
    protected final int bulkSize;

    private final TimeValue flushInterval;
    protected final Object threadLock = Boolean.TRUE;
    protected BulkRequestBuilder bulkRequest;
    protected boolean flushing;

    private final Cache<String, Integer> indexedNumberByHash;
    private final Cache<String, Integer> deletedNumberByHash;

    @Inject
    public AbstractBlockchainListenerService(String loggerName,
                                             Duniter4jClient client,
                                             PluginSettings settings,
                                             CryptoService cryptoService,
                                             ThreadPool threadPool,
                                             TimeValue bulkFlushInterval) {
        super(loggerName, client, settings, cryptoService);
        this.listenerId = loggerName;
        this.threadPool = threadPool;

        this.bulkSize = pluginSettings.getIndexBulkSize();
        this.bulkRequest = client.prepareBulk();

        this.flushInterval = bulkFlushInterval;
        this.flushing = false;

        this.indexedNumberByHash = CacheBuilder.newBuilder()
            .expireAfterWrite(bulkFlushInterval.millis() * 10, TimeUnit.MILLISECONDS)
            .build();
        this.deletedNumberByHash = CacheBuilder.newBuilder()
            .expireAfterWrite(bulkFlushInterval.millis() * 10, TimeUnit.MILLISECONDS)
            .build();
    }


    @Override
    public String getId() {
        return listenerId;
    }

    @Override
    public void onChange(ChangeEvent change) {

        // Skip if event on 'current' block
        if(BlockRepository.CURRENT_BLOCK_ID.equals(change.getId())) return;

        switch (change.getOperation()) {
            // on INDEX
            case CREATE:
            case INDEX:
                if (change.getSource() != null) {
                    synchronized (threadLock) {
                        processBlockIndex(change);
                    }
                }
                break;

            // on DELETE
            case DELETE:
                synchronized (threadLock) {
                    processBlockDelete(change);
                }
                break;
        }

    }

    @Override
    public Collection<ChangeSource> getChangeSources() {
        return CHANGE_LISTEN_SOURCES;
    }

    /* -- internal method -- */


    protected abstract void processBlockIndex(ChangeEvent change);

    protected abstract void processBlockDelete(ChangeEvent change);

    protected void flushBulkRequestOrSchedule() {
        if (bulkRequest.numberOfActions() == 0) return;

        // Flush now, if bulk is full
        if (bulkRequest.numberOfActions() % bulkSize == 0) {
            flushBulk();
            flushing = false;
        }
        else if (!flushing) {
            flushing = true;
            // Flush later (after the current block processing)
            threadPool.schedule(() -> {
                if (flushing) {
                    try {
                        flushBulk();
                    } finally {
                        flushing = false;
                    }
                }
            }, flushInterval);
        }
    }

    protected void flushBulk() {
        if (bulkRequest.numberOfActions() != 0) {
            synchronized (threadLock) {
                if (bulkRequest.numberOfActions() != 0) {
                    client.flushBulk(bulkRequest);
                    bulkRequest = client.prepareBulk();
                }
            }
        }
    }

    protected BlockchainBlock readBlock(ChangeEvent change) {
        Preconditions.checkNotNull(change);
        return readBlock(change.getSource(), change.getId());
    }

    protected BlockchainBlock readBlock(BytesReference source, Object blockNumber) {
        Preconditions.checkNotNull(source);

        try {
            return getObjectMapper().readValue(source.streamInput(), BlockchainBlock.class);
        } catch (IOException e) {
            throw new TechnicalException(String.format("Unable to parse received block #%s", blockNumber), e);
        }
    }

    protected boolean isAlreadyIndexed(BlockchainBlock block) {
        Preconditions.checkNotNull(block);
        Preconditions.checkNotNull(block.getHash());
        Preconditions.checkNotNull(block.getNumber());

        // Check if not already processed (read the indexed cache)
        Integer number = this.indexedNumberByHash.getIfPresent(block.getHash());
        return Objects.equals(number, block.getNumber());
    }

    protected void markAsIndexed(BlockchainBlock block) {
        // Add to indexed cache
        this.indexedNumberByHash.put(block.getHash(), block.getNumber());

        // Remove from the deleted cache
        this.deletedNumberByHash.invalidate(block.getHash());
    }

    protected boolean isAlreadyDeleted(BlockchainBlock block) {
        Preconditions.checkNotNull(block);
        Preconditions.checkNotNull(block.getHash());
        Preconditions.checkNotNull(block.getNumber());

        // Check if not already processed (read the deleted cache)
        Integer number = this.deletedNumberByHash.getIfPresent(block.getHash());
        return Objects.equals(number, block.getNumber());
    }

    protected void markAsDeleted(BlockchainBlock block) {
        // Add to deleted cache
        this.deletedNumberByHash.put(block.getHash(), block.getNumber());

        // Remove from the indexed cache
        this.indexedNumberByHash.invalidate(block.getHash());
    }
}
