package org.duniter.elasticsearch.service;

/*
 * #%L
 * Duniter4j :: Core API
 * %%
 * Copyright (C) 2014 - 2015 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.duniter.core.client.model.bma.BlockchainBlock;
import org.duniter.core.service.CryptoService;
import org.duniter.core.util.Preconditions;
import org.duniter.elasticsearch.PluginSettings;
import org.duniter.elasticsearch.client.Duniter4jClient;
import org.duniter.elasticsearch.dao.BlockStatRepository;
import org.duniter.elasticsearch.dao.MovementRepository;
import org.duniter.elasticsearch.model.blockchain.BlockchainBlockStat;
import org.duniter.elasticsearch.model.blockchain.Movement;
import org.duniter.elasticsearch.model.blockchain.Movements;
import org.duniter.elasticsearch.service.changes.ChangeEvent;
import org.duniter.elasticsearch.service.changes.ChangeService;
import org.duniter.elasticsearch.threadpool.ThreadPool;
import org.duniter.elasticsearch.util.bytes.JsonNodeBytesReference;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.unit.TimeValue;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Benoit on 26/04/2017.
 */
public class BlockchainListenerService extends AbstractBlockchainListenerService {

    private final BlockStatRepository blockStatRepository;
    private final MovementRepository movementRepository;

    private final boolean enableMovementIndexation;
    private final Collection<Pattern> txIncludesCommentPatterns;
    private final Collection<Pattern> txExcludesCommentPatterns;

    @Inject
    public BlockchainListenerService(Duniter4jClient client,
                                     PluginSettings settings,
                                     CryptoService cryptoService,
                                     ThreadPool threadPool,
                                     BlockStatRepository blockStatRepository,
                                     MovementRepository movementRepository) {
        super("duniter.blockchain.listener", client, settings, cryptoService, threadPool,
                new TimeValue(500, TimeUnit.MILLISECONDS));
        this.blockStatRepository = blockStatRepository;
        this.movementRepository = movementRepository;

        this.enableMovementIndexation = settings.enableMovementIndexation();

        // Include/Exclude TX by comment patterns
        this.txIncludesCommentPatterns = compilePatternsOrNull(settings.getMovementIncludesComment());
        this.txExcludesCommentPatterns = compilePatternsOrNull(settings.getMovementExcludesComment());

        if (settings.enableBlockchainIndexation()) {
            ChangeService.registerListener(this);
        }
    }

    @Override
    protected void processBlockIndex(ChangeEvent change) {
        BlockchainBlock block = readBlock(change);

        // Skip if already processed, or mark as processed
        // This is need to avoid infinite loop, when the Pod listening itself (bad network/endpoints configuration)
        if (isAlreadyIndexed(block)) {
            if (logger.isDebugEnabled()) logger.debug("Skipping already processed block #{}-{}", block.getNumber(), block.getHash());
            return;
        }
        this.markAsIndexed(block);

        processBlock(block);
    }

    protected void processBlockDelete(ChangeEvent change) {
        // Block stat
        {
            // Add delete to bulk
            bulkRequest.add(client.prepareDelete(change.getIndex(), BlockStatRepository.TYPE, change.getId())
                    .setRefresh(false));
        }

        // Movements
        {
            // Add delete to bulk
            bulkRequest = movementRepository.bulkDeleteByBlock(
                    change.getIndex(),
                    change.getId(),
                    null/*do kwown the hash*/,
                    bulkRequest, bulkSize, false);
            flushBulkRequestOrSchedule();
        }
    }

    /* -- internal method -- */


    protected void processBlock(BlockchainBlock block) {
        ObjectMapper objectMapper = getObjectMapper();

        // Block stat
        {
            BlockchainBlockStat stat = blockStatRepository.toBlockStat(block);

            // Add a delete to bulk
            bulkRequest.add(client.prepareDelete(block.getCurrency(), BlockStatRepository.TYPE, String.valueOf(block.getNumber()))
                .setRefresh(false));
            flushBulkRequestOrSchedule();

            // Add an insertion to bulk
            try {
                bulkRequest.add(client.prepareIndex(block.getCurrency(), BlockStatRepository.TYPE, String.valueOf(block.getNumber()))
                    .setRefresh(false) // recommended for heavy indexing
                    .setSource(objectMapper.writeValueAsBytes(stat)));
                flushBulkRequestOrSchedule();
            } catch (JsonProcessingException e) {
                logger.error("Could not serialize BlockStat into JSON: " + e.getMessage(), e);
            }
        }

        // Movements
        if (enableMovementIndexation) {

            // Delete previous indexation
            bulkRequest = movementRepository.bulkDeleteByBlock(block.getCurrency(),
                String.valueOf(block.getNumber()),
                null, /*do NOT filter on hash = delete by block number*/
                bulkRequest, bulkSize, false);

            // Add a insert to bulk
            Movements.stream(block)
                .filter(this::filterMovement)
                .forEach(movement -> {
                    try {
                        bulkRequest.add(client.prepareIndex(block.getCurrency(), MovementRepository.TYPE)
                            .setRefresh(false) // recommended for heavy indexing
                            .setSource(new JsonNodeBytesReference(movement, objectMapper)));
                        flushBulkRequestOrSchedule();
                    } catch (IOException e) {
                        logger.error("Could not serialize Movement into JSON: " + e.getMessage(), e);
                    }
                });
        }

    }

    protected Collection<Pattern> compilePatternsOrNull(String[] patterns) {
        return ArrayUtils.isEmpty(patterns) ? null :
                Arrays.stream(patterns)
                        .filter(StringUtils::isNotBlank)
                        .map(p -> "^" + p.replaceAll("[*]", ".*") + "$")
                        .map(Pattern::compile)
                        .collect(Collectors.toSet());
    }

    protected boolean filterMovement(Movement movement) {
        if (this.txIncludesCommentPatterns == null && this.txExcludesCommentPatterns == null) {
            return true;
        }
        String comment = StringUtils.trimToNull(movement.getComment());

        Boolean included = (this.txIncludesCommentPatterns == null) ? null :
                ((comment == null) ? Boolean.FALSE :
                        this.txIncludesCommentPatterns.stream()
                                .filter(pattern -> pattern.matcher(comment).matches()).map(p -> Boolean.TRUE)
                                .findFirst().orElse(Boolean.FALSE));
        Boolean excluded = (this.txExcludesCommentPatterns == null) ? null :
                ((comment == null) ? Boolean.FALSE :
                this.txExcludesCommentPatterns.stream()
                    .filter(pattern -> pattern.matcher(comment).matches()).map(p -> Boolean.TRUE)
                    .findFirst().orElse(Boolean.FALSE));

        boolean result = !Objects.equals(included, Boolean.FALSE) && !Objects.equals(excluded, Boolean.TRUE);
        return result;
    }
}
