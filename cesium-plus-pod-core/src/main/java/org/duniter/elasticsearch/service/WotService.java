package org.duniter.elasticsearch.service;

/*
 * #%L
 * Duniter4j :: Core API
 * %%
 * Copyright (C) 2014 - 2015 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.duniter.core.client.model.bma.BlockchainParameters;
import org.duniter.core.client.model.bma.WotRequirements;
import org.duniter.core.client.model.local.Identity;
import org.duniter.core.client.model.local.Member;
import org.duniter.core.client.repositories.CurrencyRepository;
import org.duniter.core.client.service.bma.WotRemoteService;
import org.duniter.core.util.*;
import org.duniter.elasticsearch.PluginSettings;
import org.duniter.elasticsearch.client.Duniter4jClient;
import org.duniter.elasticsearch.dao.BlockRepository;
import org.duniter.elasticsearch.dao.CurrencyExtendRepository;
import org.duniter.elasticsearch.dao.MemberRepository;
import org.duniter.elasticsearch.dao.PendingMembershipRepository;
import org.duniter.elasticsearch.service.changes.ChangeEvent;
import org.duniter.elasticsearch.service.changes.ChangeService;
import org.duniter.elasticsearch.service.changes.ChangeSource;
import org.duniter.elasticsearch.threadpool.ThreadPool;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import java.io.Closeable;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Benoit on 30/03/2015.
 */
public class WotService extends AbstractService {

    private static final String LOCK_NAME_COMPUTE_MEMBERS = "Index WoT members";


    private BlockRepository blockRepository;
    private MemberRepository memberRepository;
    private CurrencyExtendRepository currencyRepository;
    private PendingMembershipRepository pendingMembershipRepository;
    private WotRemoteService wotRemoteService;
    private BlockchainService blockchainService;
    private ThreadPool threadPool;
    private final LockManager lockManager = new LockManager(1, 4);
    private final Map<String, ChangeService.ChangeListener> currentBlockListeners = Maps.newConcurrentMap();

    private final Map<String, Boolean> isBlockchainIndexationReady = Maps.newConcurrentMap();

    @Inject
    public WotService(Duniter4jClient client,
                      PluginSettings settings,
                      ThreadPool threadPool,
                      BlockRepository blockRepository,
                      MemberRepository memberRepository,
                      CurrencyRepository currencyRepository,
                      PendingMembershipRepository pendingMembershipRepository,
                      BlockchainService blockchainService,
                      final ServiceLocator serviceLocator){
        super("duniter.wot", client, settings);
        this.client = client;
        this.blockRepository = blockRepository;
        this.memberRepository = memberRepository;
        this.currencyRepository = (CurrencyExtendRepository) currencyRepository;
        this.pendingMembershipRepository = pendingMembershipRepository;
        this.blockchainService = blockchainService;
        this.threadPool = threadPool;
        this.threadPool.scheduleOnStarted(() -> {
            wotRemoteService = serviceLocator.getWotRemoteService();
            setIsReady(true);
        });
    }

    public List<Member> getMembers(String currency) {

        final String currencyId = safeGetCurrency(currency);

        // Blockchain indexation is enable: use it!
        if (isBlockchainReady(currencyId)) {

            List<Member> members = memberRepository.getMembers(currencyId);

            // No members, or not indexed yet ?
            if (CollectionUtils.isEmpty(members)) {
                logger.warn("No member found. Trying to index members...");
                return indexAndGetMembers(currencyId);
            }

            return members;
        }

        // Else, fallback to the Duniter node
        else {
            return wotRemoteService.getMembers(currencyId);
        }
    }

    public void save(String currencyId, final List<Member> members) {
        // skip if nothing to save
        if (CollectionUtils.isEmpty(members)) return;

        memberRepository.save(currencyId, members);
    }

    public Member save(final Member member) {
        Preconditions.checkNotNull(member);
        Preconditions.checkNotNull(member.getCurrency());
        Preconditions.checkNotNull(member.getPubkey());
        Preconditions.checkNotNull(member.getUid());

        boolean exists = memberRepository.isExists(member.getCurrency(), member.getPubkey());

        // Create
        if (!exists) {
            memberRepository.create(member);
        }

        // or update
        else {
            memberRepository.update(member);
        }
        return member;
    }

    public WotService indexMembers(final String currency) {
        indexAndGetMembers(currency);
        return this;
    }

    public WotService stopListenAndIndexMembers(final String currency) {
        ChangeService.ChangeListener listener = currentBlockListeners.remove(currency);
        if (listener != null) {
            ChangeService.unregisterListener(listener);
        }
        return this;
    }

    public Closeable listenAndIndexMembers(final String currency) {
        // Stop if previous listener was existing
        stopListenAndIndexMembers(currency);

        // Listen changes on block
        ChangeService.ChangeListener listener =  ChangeService.registerListener(new ChangeService.ChangeListener() {
            @Override
            public String getId() {
                return "duniter.wot";
            }
            @Override
            public Collection<ChangeSource> getChangeSources() {
                return ImmutableList.of(new ChangeSource(currency, BlockRepository.TYPE, "current"));
            }
            @Override
            public void onChange(ChangeEvent change) {
                // If current block indexed
                switch (change.getOperation()) {
                    case CREATE:
                    case INDEX:
                        logger.debug(String.format("[%s] Scheduling indexation of WoT members", currency));
                        threadPool.schedule(() -> {
                            try {
                                // Acquire lock (once members indexation at a time)
                                if (lockManager.tryLock(LOCK_NAME_COMPUTE_MEMBERS, 10, TimeUnit.SECONDS)) {
                                    try {
                                        indexMembers(currency);
                                    }
                                    catch (Exception e) {
                                        logger.error("Error while indexing WoT members: " + e.getMessage(), e);
                                    }
                                    finally {
                                        // Release the lock
                                        lockManager.unlock(LOCK_NAME_COMPUTE_MEMBERS);
                                    }
                                }
                                else {
                                    logger.debug("Could not acquire lock for indexing members. Skipping.");
                                }
                            } catch (InterruptedException e) {
                                logger.warn("Stopping indexation of WoT members: " + e.getMessage());
                            }
                        }, 30, TimeUnit.SECONDS);
                        break;
                    default:
                        // Skip deletion
                        break;
                }

            }
        });

        this.currentBlockListeners.put(currency, listener);

        // Return the tear down logic
        return () -> this.stopListenAndIndexMembers(currency);
    }

    public boolean isOrWasMember(String pubkey) {
        return Beans.getStream(currencyRepository.findAllIds())
            .anyMatch(currencyId -> this.isOrWasMember(currencyId, pubkey));
    }

    public boolean isOrWasMember(String currency, String pubkey) {
        final String currencyId = safeGetCurrency(currency);
        if (StringUtils.isBlank(currencyId)) return false;

        SearchResponse response = client.prepareSearch()
            .setIndices(currencyId)
            .setSize(0) // only need the total
            .setTypes(MemberRepository.TYPE)
            .setQuery(QueryBuilders.idsQuery().ids(pubkey))
            .setRequestCache(true)
            .execute().actionGet();

        return response.getHits() != null && response.getHits().getTotalHits() > 0;
    }

    public boolean isMember(String pubkey) {
        return Beans.getStream(currencyRepository.findAllIds())
            .anyMatch(currencyId -> this.isMember(currencyId, pubkey));
    }

    public boolean isMember(String currency, String pubkey) {

        final String currencyId = safeGetCurrency(currency);

        QueryBuilder query = QueryBuilders.constantScoreQuery(QueryBuilders.boolQuery()
            .filter(QueryBuilders.idsQuery().addIds(pubkey))
            .filter(QueryBuilders.termQuery(Member.Fields.IS_MEMBER, true))
        );

        SearchResponse response = client.prepareSearch()
            .setIndices(currencyId)
            .setSize(0) // only need the total
            .setTypes(MemberRepository.TYPE)
            .setQuery(query)
            .setRequestCache(true)
            .execute().actionGet();

        return response.getHits() != null && response.getHits().getTotalHits() > 0;
    }

    public List<WotRequirements> getRequirements(String currency, String uidOrPubkey) {
        waitReady();
        final String currencyId = safeGetCurrency(currency);
        return this.wotRemoteService.getRequirements(currencyId, uidOrPubkey);
    }

    public List<WotRequirements> getRequirementsByPubkey(String currency, String pubkey) {
        waitReady();
        final String currencyId = safeGetCurrency(currency);
        return this.wotRemoteService.getRequirementsByPubkey(currencyId, pubkey);
    }

    public Optional<Member> getMemberByPubkey(String currency, String pubkey) {
        final String currencyId = safeGetCurrency(currency);

        if (isBlockchainReady(currencyId)) {
            return this.memberRepository.getMemberByPubkey(currencyId, pubkey);
        }
        // Fallback to remote duniter node (e.g. Blockchain not yet indexed)
        else {
            return this.wotRemoteService.getRequirementsByPubkey(currencyId, pubkey)
                .stream()
                .map(source -> {
                    Member target = new Member();
                    target.setPubkey(pubkey);
                    target.setCurrency(currencyId);
                    target.setUid(source.getUid());
                    target.setWasMember(source.getWasMember());
                    Boolean isMember = Boolean.FALSE.equals(source.getRevoked())
                        && source.getMembershipExpiresIn() != null
                        && source.getMembershipExpiresIn() > 0;
                    target.setIsMember(isMember);
                    if (source.getMeta() != null) {
                        target.setTimestamp(source.getMeta().getTimestamp());
                    }
                    return target;
                })
                .filter(source -> Boolean.TRUE.equals(source.getWasMember()) || source.getIsMember())
                .findFirst();
        }
    }

    /* -- protected methods -- */

    protected List<Member> indexAndGetMembers(final String currencyId) {

        logger.info(String.format("[%s] Indexing WoT members...", currencyId));

        final BlockchainParameters parameters = blockchainService.getParameters(currencyId);

        // Retrieve previous members pubkeys. This list will be reduce later, to keep only excluded members
        final Set<String> pubkeysToExclude = memberRepository.getMemberPubkeys(currencyId);
        final long previousMembersCount = CollectionUtils.size(pubkeysToExclude);

        final List<Member> members = blockRepository.getMembers(parameters);

        // If cannot compute (e.g. no current block): skip
        if (members == null) return ImmutableList.of();

        // Save members into index
        final MutableInt becomesCount = new MutableInt(0);
        if (CollectionUtils.isNotEmpty(members)) {
            // Set currency
            members.forEach(m -> {
                // Remove from the list
                boolean becomeMember = !pubkeysToExclude.remove(m.getPubkey());
                // If not found in the previous list = new member
                if(becomeMember) becomesCount.increment();
                m.setCurrency(currencyId);
            });
        }

        int excludedCount = CollectionUtils.size(pubkeysToExclude);
        long deltaCount = CollectionUtils.size(members) - previousMembersCount;
        boolean hasBecomes = becomesCount.getValue() > 0;
        boolean hasExcluded = excludedCount > 0;
        boolean hasChanges = deltaCount != 0 || hasBecomes || hasExcluded;

        // Has changes
        if (hasChanges) {

            // Save members
            if (hasBecomes) {
                memberRepository.save(currencyId, members);
            }

            // Update old members as "was member"
            if (hasExcluded) {
                memberRepository.updateAsWasMember(currencyId, pubkeysToExclude);
            }

            // Update currency member count
            if (deltaCount != 0) {
                currencyRepository.updateMemberCount(currencyId, members.size());
            }

            logger.info(String.format("[%s] Indexing WoT members [OK] - %s members (%s%s), %s becomes, %s excluded", currencyId,
                    CollectionUtils.size(members),
                    (deltaCount > 0) ? "\u21D1" : "\u21D3",
                    Math.abs(deltaCount),
                    becomesCount.getValue(),
                    excludedCount));
        }

        // No changes: just log
        else {
            logger.info(String.format("[%s] Indexing WoT members [OK] - %s members (unchanged)", currencyId, CollectionUtils.size(members)));
        }

        return members;
    }

    /**
     * Return the given currency, or the default currency
     * @param currency
     * @return
     */
    protected String safeGetCurrency(String currency) {
        if (StringUtils.isNotBlank(currency)) return currency;
        return currencyRepository.getDefaultId();
    }

    protected boolean isBlockchainReady(String currency) {
        if (!this.isReady()) return false;

        Boolean isReady = isBlockchainIndexationReady.get(currency);
        if (isReady != null) return isReady.booleanValue();

        // Blockchain indexation was disable in settings
        if (!pluginSettings.enableBlockchainIndexation()) {
            isBlockchainIndexationReady.put(currency, Boolean.FALSE);
            return false;
        }

        // Check if there is a current block
        else {
            try {
                boolean hasCurrentBlock = blockchainService.getCurrentBlock(currency) != null;
                if (hasCurrentBlock) {

                    // OK. Remember that indexation is ready
                    isBlockchainIndexationReady.put(currency, Boolean.TRUE);
                    return true;
                }
            }
            catch(Throwable t) {
            }

            // No current block => indexation is still processing
            // do NOT set the map, to force new check later
            return false;
        }
    }
}
