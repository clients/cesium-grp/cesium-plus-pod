package org.duniter.elasticsearch.service;

/*
 * #%L
 * Duniter4j :: Core API
 * %%
 * Copyright (C) 2014 - 2015 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.duniter.elasticsearch.PluginSettings;
import org.duniter.elasticsearch.client.Duniter4jClient;
import org.duniter.elasticsearch.dao.RequestLogRepository;
import org.duniter.elasticsearch.model.RequestLog;
import org.elasticsearch.common.inject.Inject;

/**
 * Maintained stats on doc (count records)
 * Created by Benoit on 30/03/2015.
 */
public class RequestLogService extends AbstractService  {

    private RequestLogRepository repository;

    @Inject
    public RequestLogService(Duniter4jClient client, PluginSettings settings,
                             RequestLogRepository repository){
        super("duniter.log.request", client, settings);
        this.repository = repository;
    }

    public RequestLogService createIndexIfNotExists() {
        repository.createIndexIfNotExists();
        return this;
    }

    public RequestLogService deleteIndex() {
        repository.deleteIndex();
        return this;
    }

    public void save(RequestLog request) {
        repository.prepareIndex(request).execute();
    }

}
