package org.duniter.elasticsearch.dao;

/*-
 * #%L
 * Cesium+ pod :: Core plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.duniter.core.client.model.local.Peer;
import org.duniter.core.client.repositories.PeerRepository;
import org.elasticsearch.index.query.QueryBuilder;

public interface PeerExtendRepository extends PeerRepository, TypeRepository<PeerExtendRepository> {

    String TYPE = "peer";

    Peer create(Peer peer);

    Peer update(Peer peer);

    long countByCurrency(String currency, QueryBuilder query);

    void deleteByCurrencyAndId(String currency, String id);
}
