package org.duniter.elasticsearch.dao.impl;

/*
 * #%L
 * Duniter4j :: Core API
 * %%
 * Copyright (C) 2014 - 2015 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.fasterxml.jackson.core.JsonProcessingException;
import org.duniter.core.exception.TechnicalException;
import org.duniter.core.util.Preconditions;
import org.duniter.core.util.StringUtils;
import org.duniter.elasticsearch.PluginSettings;
import org.duniter.elasticsearch.dao.AbstractIndexTypeRepository;
import org.duniter.elasticsearch.dao.RequestLogRepository;
import org.duniter.elasticsearch.model.RequestLog;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.IOException;

/**
 * Created by blavenie on 16/05/22
 */
public class RequestLogRepositoryImpl extends AbstractIndexTypeRepository<RequestLogRepository> implements RequestLogRepository {

    private PluginSettings pluginSettings;

    @Inject
    public RequestLogRepositoryImpl(PluginSettings pluginSettings) {
        super(RequestLogRepository.INDEX, RequestLogRepository.TYPE);
        this.pluginSettings = pluginSettings;
    }

    @Override
    public String getType() {
        return TYPE;
    }


    @Override
    public IndexRequestBuilder prepareIndex(RequestLog request) {
        Preconditions.checkNotNull(request);
        Preconditions.checkArgument(StringUtils.isNotBlank(request.getHost()));
        Preconditions.checkArgument(StringUtils.isNotBlank(request.getPath()));
        Preconditions.checkArgument(StringUtils.isNotBlank(request.getMethod()));

        // Make sure time has been set
        if (request.getTime() == 0) {
            request.setTime(System.currentTimeMillis()/1000);
        }

        try {
            return this.client.prepareIndex(INDEX, TYPE)
                .setRefresh(false)
                .setSource(getObjectMapper().writeValueAsBytes(request));
        }
        catch(JsonProcessingException e) {
            throw new TechnicalException(e);
        }
    }

    @Override
    protected void createIndex() throws JsonProcessingException {
        logger.info(String.format("Creating index [%s]", INDEX));

        client.admin().indices().prepareCreate(INDEX)
            .setSettings(Settings.settingsBuilder()
                .put("number_of_shards", 3)
                .put("number_of_replicas", 1)
                .build())
            .addMapping(TYPE, createTypeMapping())
            .execute().actionGet();
    }

    @Override
    public XContentBuilder createTypeMapping() {
        try {
            XContentBuilder mapping = XContentFactory.jsonBuilder()
                .startObject()
                .startObject(TYPE)
                .startObject("properties")

                // host
                .startObject(RequestLog.Fields.HOST)
                .field("type", "string")
                .field("index", "not_analyzed")
                .endObject()

                // method
                .startObject(RequestLog.Fields.METHOD)
                .field("type", "string")
                .field("index", "not_analyzed")
                .endObject()

                // path
                .startObject(RequestLog.Fields.PATH)
                .field("type", "string")
                .field("index", "not_analyzed")
                .endObject()

                // time
                .startObject(RequestLog.Fields.TIME)
                .field("type", "integer")
                .endObject()

                .endObject()
                .endObject().endObject();

            return mapping;
        }
        catch(IOException ioe) {
            throw new TechnicalException(String.format("Error while getting mapping for index %s: %s", TYPE, ioe.getMessage()), ioe);
        }
    }
}
