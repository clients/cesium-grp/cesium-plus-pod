package org.duniter.elasticsearch.dao.impl;

/*
 * #%L
 * UCoin Java :: Core Client API
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import org.duniter.core.client.model.local.Peer;
import org.duniter.core.exception.TechnicalException;
import org.duniter.core.util.Preconditions;
import org.duniter.core.util.StringUtils;
import org.duniter.elasticsearch.dao.AbstractRepository;
import org.duniter.elasticsearch.dao.SynchroExecutionRepository;
import org.duniter.elasticsearch.model.network.SynchroExecution;
import org.duniter.elasticsearch.model.network.SynchroResult;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;

import java.io.IOException;

/**
 * Created by blavenie on 29/12/15.
 */
public class SynchroExecutionRepositoryImpl extends AbstractRepository implements SynchroExecutionRepository {

    public SynchroExecutionRepositoryImpl(){
        super("duniter.dao.peer");
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public void save(SynchroExecution execution) {
        Preconditions.checkNotNull(execution);
        Preconditions.checkArgument(StringUtils.isNotBlank(execution.getCurrency()));
        Preconditions.checkArgument(StringUtils.isNotBlank(execution.getPeer()));
        Preconditions.checkNotNull(execution.getTime());
        Preconditions.checkArgument(execution.getTime() > 0);

        try {
            // Serialize into JSON
            String json = getObjectMapper().writeValueAsString(execution);

            // Preparing indexBlocksFromNode
            IndexRequestBuilder indexRequest = client.prepareIndex(execution.getCurrency(), TYPE)
                    .setSource(json);

            // Execute indexBlocksFromNode
            indexRequest
                    .setRefresh(true)
                    .execute().actionGet();
        }
        catch(JsonProcessingException e) {
            throw new TechnicalException(e);
        }
    }

    @Override
    public SynchroExecution getLastExecution(Peer peer) {
        Preconditions.checkNotNull(peer);
        Preconditions.checkNotNull(peer.getCurrency());
        Preconditions.checkNotNull(peer.getId());
        Preconditions.checkNotNull(peer.getApi());

        String hash = peer.getHash();
        if (StringUtils.isBlank(hash)) {
            hash = cryptoService.hash(peer.toString());
            peer.setHash(hash);
            if (logger.isDebugEnabled()) {
                logger.debug(String.format("[%s] [%s] Computing missing hash for {%s}: %s", peer.getCurrency(), peer.getApi(), peer, hash));
            }
        }

        BoolQueryBuilder query = QueryBuilders.boolQuery()
                .filter(QueryBuilders.termQuery(SynchroExecution.Fields.PEER, peer.getHash()))
                .filter(QueryBuilders.termQuery(SynchroExecution.Fields.API, peer.getApi()));

        SearchResponse response = client.prepareSearch(peer.getCurrency())
                .setTypes(TYPE)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(query)
                .setFetchSource(true)
                .setSize(1)
                .addSort(SynchroExecution.Fields.TIME, SortOrder.DESC)
                .get();

        if (response.getHits().getTotalHits() == 0) return null;

        SearchHit hit = response.getHits().getHits()[0];
        return client.readSourceOrNull(hit, SynchroExecution.class);
    }

    @Override
    public XContentBuilder createTypeMapping() {
        try {
            XContentBuilder mapping = XContentFactory.jsonBuilder()
                    .startObject()
                    .startObject(TYPE)
                    .startObject("properties")

                    // currency
                    .startObject(SynchroExecution.Fields.CURRENCY)
                    .field("type", "string")
                    .endObject()

                    // peer
                    .startObject(SynchroExecution.Fields.PEER)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // peer
                    .startObject(SynchroExecution.Fields.API)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // issuer
                    .startObject(SynchroExecution.Fields.ISSUER)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // time
                    .startObject(SynchroExecution.Fields.TIME)
                    .field("type", "long")
                    .endObject()

                    // hash
                    .startObject(SynchroExecution.Fields.HASH)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // signature
                    .startObject(SynchroExecution.Fields.SIGNATURE)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // execution time
                    .startObject(SynchroExecution.Fields.EXECUTION_TIME)
                    .field("type", "long")
                    .endObject()

                    // result
                    .startObject(SynchroExecution.Fields.RESULT)
                    .field("type", "nested")
                    .field("dynamic", "false")
                    .startObject("properties")

                        // inserts
                        .startObject(SynchroResult.Fields.INSERTS)
                        .field("type", "long")
                        .endObject()

                        // updates
                        .startObject(SynchroResult.Fields.UPDATES)
                        .field("type", "long")
                        .endObject()

                        // deletes
                        .startObject(SynchroResult.Fields.DELETES)
                        .field("type", "long")
                        .endObject()

                        // invalid signatures
                        .startObject(SynchroResult.Fields.INVALID_SIGNATURES)
                        .field("type", "long")
                        .endObject()

                        // invalid times
                        .startObject(SynchroResult.Fields.INVALID_TIMES)
                        .field("type", "long")
                        .endObject()

                    .endObject()
                    .endObject()

                    .endObject()
                    .endObject().endObject();

            return mapping;
        }
        catch(IOException ioe) {
            throw new TechnicalException("Error while getting mapping for synchro index: " + ioe.getMessage(), ioe);
        }
    }
}
