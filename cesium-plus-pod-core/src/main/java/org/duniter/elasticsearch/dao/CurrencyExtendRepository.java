package org.duniter.elasticsearch.dao;

/*-
 * #%L
 * Duniter4j :: ElasticSearch Core plugin
 * %%
 * Copyright (C) 2014 - 2017 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.duniter.core.client.repositories.CurrencyRepository;
import org.duniter.elasticsearch.model.blockchain.Currency;

/**
 * Created by blavenie on 03/04/17.
 */
public interface CurrencyExtendRepository extends
    CurrencyRepository<Currency>, IndexTypeRepository<CurrencyExtendRepository, String> {

    String INDEX = "currency";
    String RECORD_TYPE = "record";


    String getDefaultId();

    Iterable<String> findAllIds();

    /**
     * Update the currency member count
     * @param currency
     * @param memberCount
     */
    void updateMemberCount(String currency, int memberCount);

    /**
     * Update the currency last dividend
     * @param currency
     * @param dividend
     */
    void updateDividend(String currency, long dividend);

    Currency create(Currency currency);

    Currency update(Currency currency);

}
