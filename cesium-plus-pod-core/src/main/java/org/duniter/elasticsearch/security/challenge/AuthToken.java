package org.duniter.elasticsearch.security.challenge;

/*-
 * #%L
 * Cesium+ pod :: Core plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.duniter.core.util.Preconditions;

public class AuthToken {
    public String pubkey;
    public String challenge;
    public String signature;


    public String toString() {
        return pubkey + ":" + challenge + "|" + signature;
    }

    public static AuthToken parse(String token) {
        Preconditions.checkNotNull(token);
        String[] parts = token.split(":");
        Preconditions.checkArgument(parts.length == 2);
        String[] subParts = parts[1].split("|");
        Preconditions.checkArgument(subParts.length == 2);

        AuthToken target = new AuthToken();
        target.pubkey = parts[0];
        target.challenge = subParts[0];
        target.signature = subParts[1];
        return target;
    }
}
