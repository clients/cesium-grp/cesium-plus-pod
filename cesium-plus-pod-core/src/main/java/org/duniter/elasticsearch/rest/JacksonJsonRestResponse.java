package org.duniter.elasticsearch.rest;

/*-
 * #%L
 * Cesium+ pod :: Core plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.http.entity.ContentType;
import org.duniter.core.client.model.bma.jackson.JacksonUtils;
import org.elasticsearch.rest.BytesRestResponse;
import org.elasticsearch.rest.RestRequest;
import org.elasticsearch.rest.RestStatus;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class JacksonJsonRestResponse extends BytesRestResponse {

    public JacksonJsonRestResponse(RestRequest request, RestStatus status, Object result) throws IOException {
        super(status, ContentType.APPLICATION_JSON.toString(), getBytes(request, result));
    }

    public static byte[] getBytes(RestRequest request, Object result) throws IOException {

        boolean pretty = request.hasParam("pretty");
        ObjectWriter writer;
        ObjectMapper mapper = JacksonUtils.getThreadObjectMapper();
        if (pretty) {
            writer = mapper.writerWithDefaultPrettyPrinter();
        }
        else {
            writer = mapper.writer();
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        writer.writeValue(bos, result);

        return bos.toByteArray();
    }
}
