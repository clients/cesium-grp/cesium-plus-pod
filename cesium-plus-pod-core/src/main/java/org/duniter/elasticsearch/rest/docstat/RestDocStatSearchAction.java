package org.duniter.elasticsearch.rest.docstat;

/*-
 * #%L
 * Cesium+ pod :: Core plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.duniter.elasticsearch.PluginSettings;
import org.duniter.elasticsearch.dao.DocStatRepository;
import org.duniter.elasticsearch.rest.security.RestSecurityController;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.rest.BaseRestHandler;
import org.elasticsearch.rest.RestChannel;
import org.elasticsearch.rest.RestController;
import org.elasticsearch.rest.RestRequest;
import org.elasticsearch.rest.action.search.RestSearchAction;
import org.elasticsearch.rest.action.support.RestStatusToXContentListener;

import static org.elasticsearch.rest.RestRequest.Method.GET;
import static org.elasticsearch.rest.RestRequest.Method.POST;

/**
 * /docstat/record has been replaced by /message/inbox
 * @deprecated
 */
@Deprecated
public class RestDocStatSearchAction extends BaseRestHandler {

    @Inject
    public RestDocStatSearchAction(Settings settings, RestController controller, Client client,
                                   PluginSettings pluginSettings,
                                   RestSecurityController securityController) {
        super(settings, controller, client);

        if (pluginSettings.enableDocStats()) {
            securityController.allowGetSearchIndexType(DocStatRepository.OLD_INDEX, DocStatRepository.OLD_TYPE);
            securityController.allowPostSearchIndexType(DocStatRepository.OLD_INDEX, DocStatRepository.OLD_TYPE);
            controller.registerHandler(GET, String.format("/%s/%s/_search", DocStatRepository.OLD_INDEX, DocStatRepository.OLD_TYPE), this);
            controller.registerHandler(POST, String.format("/%s/%s/_search", DocStatRepository.OLD_INDEX, DocStatRepository.OLD_TYPE), this);
        }
    }

    @Override
    public void handleRequest(final RestRequest request, final RestChannel channel, final Client client) {
        SearchRequest searchRequest = new SearchRequest();
        RestSearchAction.parseSearchRequest(searchRequest, request, parseFieldMatcher, null);

        // Redirect to new index/type
        searchRequest.indices(DocStatRepository.INDEX).types(DocStatRepository.TYPE);

        client.search(searchRequest, new RestStatusToXContentListener<SearchResponse>(channel));
    }
}
