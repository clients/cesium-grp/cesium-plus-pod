package org.duniter.elasticsearch.rest.log;

/*-
 * #%L
 * Cesium+ pod :: Core plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.net.HttpHeaders;
import org.duniter.elasticsearch.PluginSettings;
import org.duniter.elasticsearch.dao.RequestLogRepository;
import org.duniter.elasticsearch.rest.security.RestSecurityController;
import org.duniter.elasticsearch.security.token.SecurityTokenStore;
import org.duniter.elasticsearch.util.RestUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.logging.Loggers;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.rest.*;
import org.elasticsearch.rest.action.search.RestSearchAction;
import org.elasticsearch.rest.action.support.RestStatusToXContentListener;

import static org.elasticsearch.rest.RestRequest.Method.GET;
import static org.elasticsearch.rest.RestRequest.Method.POST;
import static org.elasticsearch.rest.RestStatus.FORBIDDEN;
import static org.elasticsearch.rest.RestStatus.UNAUTHORIZED;

/**
 *
 */
public class RestLogRequestSearchAction extends BaseRestHandler {

    public final static String TOKEN = "token";

    private final ESLogger log;

    private final SecurityTokenStore securityTokenStore;

    @Inject
    public RestLogRequestSearchAction(Settings settings, RestController controller, Client client,
                                      PluginSettings pluginSettings,
                                      RestSecurityController securityController,
                                      SecurityTokenStore securityTokenStore) {
        super(settings, controller, client);
        log = Loggers.getLogger("duniter.rest." + RequestLogRepository.INDEX, settings, String.format("[%s]", RequestLogRepository.INDEX));
        this.securityTokenStore = securityTokenStore;
        if (pluginSettings.enableQuota() && pluginSettings.logRejectedRequests()) {
            securityController.allowPostSearchIndexType(RequestLogRepository.INDEX, RequestLogRepository.TYPE);
            controller.registerHandler(GET, String.format("/%s/%s/_search", RequestLogRepository.INDEX, RequestLogRepository.TYPE), this);
            controller.registerHandler(POST, String.format("/%s/%s/_search", RequestLogRepository.INDEX, RequestLogRepository.TYPE), this);
        }
    }

    @Override
    public void handleRequest(final RestRequest request, final RestChannel channel, final Client client) {
        SearchRequest searchRequest = new SearchRequest();
        RestSearchAction.parseSearchRequest(searchRequest, request, parseFieldMatcher, null);

        // Check validation token
        String authorization = request.header(HttpHeaders.AUTHORIZATION);
        if (authorization == null || !authorization.startsWith(TOKEN)) {
            channel.sendResponse(new BytesRestResponse(UNAUTHORIZED));
            return;
        }

        String token = authorization.substring(TOKEN.length()).trim();

        if (!securityTokenStore.validateToken(token)) {

            String ip = RestUtils.getIPAddress(request);
            log.warn("Reject request to [{}/{}] from {{}} - Invalid token: {}",
                RequestLogRepository.INDEX, RequestLogRepository.TYPE, ip, token);

            channel.sendResponse(new BytesRestResponse(FORBIDDEN));
            return;
        }

        log.debug("Authorized access to [{}/{}]", RequestLogRepository.INDEX, RequestLogRepository.TYPE);

        searchRequest.indices(RequestLogRepository.INDEX).types(RequestLogRepository.TYPE);
        client.search(searchRequest, new RestStatusToXContentListener<SearchResponse>(channel));
    }
}
