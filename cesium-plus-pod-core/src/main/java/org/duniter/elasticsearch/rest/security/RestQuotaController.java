package org.duniter.elasticsearch.rest.security;

/*
 * #%L
 * Duniter4j :: ElasticSearch Plugin
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.duniter.elasticsearch.PluginSettings;
import org.duniter.elasticsearch.model.RequestLog;
import org.duniter.elasticsearch.service.RequestLogService;
import org.duniter.elasticsearch.util.RestUtils;
import org.elasticsearch.common.component.AbstractLifecycleComponent;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.logging.Loggers;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.rest.RestRequest;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * Created by blavenie on 30/12/20.
 */
public class RestQuotaController extends AbstractLifecycleComponent<RestQuotaController> {

    // A regexp that should EXCLUDE all URL with pubkey, docId, etc.
    private final static Pattern CACHE_PATH_REGEXP = Pattern.compile("^[0-9a-z_/]+$");

    private final ESLogger log;

    private boolean enable;
    private boolean trace;

    private final Set<String> ipWhiteList;

    private final Set<String> ipBlackList;

    private boolean logRejectedRequests;

    private final Map<RestRequest.Method, Map<String, QuotaMap>> quotasByMethod;

    private final Map<RestRequest.Method, Set<String>> noQuotasCache;

    private final RequestLogService requestLogService;

    @Inject
    public RestQuotaController(Settings settings, PluginSettings pluginSettings, RequestLogService requestLogService) {
        super(settings);
        this.log = Loggers.getLogger("duniter.security.quota", settings, new String[0]);
        this.requestLogService = requestLogService;
        this.trace = log.isTraceEnabled();
        this.enable = pluginSettings.enableQuota();
        this.quotasByMethod = Maps.newHashMap();
        this.noQuotasCache = Maps.newHashMap();
        this.ipWhiteList = Sets.newHashSet(pluginSettings.getIpWhiteList());
        this.ipBlackList = Sets.newHashSet(pluginSettings.getIpBlackList());
        this.logRejectedRequests =  pluginSettings.logRejectedRequests();
        if (!enable) {
            log.warn("/!\\ Quota has been disabled using option [duniter.security.quota.enable]. This is NOT recommended in production !");
        }
        else {
            log.info(String.format("Starting security quota controller - {whiteList: %s}", ipWhiteList));
        }
    }

    public RestQuotaController quota(RestRequest.Method method, String regexPath, int maxCount, int duration, TimeUnit unit) {
        Map<String, QuotaMap> quotaByRequest = quotasByMethod.computeIfAbsent(method, k -> new LinkedHashMap<>());

        if (!quotaByRequest.containsKey(regexPath)) {
            // On max exceed: log the request
            QuotaMap.MaxExceededListener onMaxExceededListener = !trace && !logRejectedRequests
                ? null // No listener, when log has been disabled
                : (address) -> {
                    if (trace) log.trace(String.format("Reject %s request [%s] from address {%s} - maxCallCount: %s, duration: '%s%s'",
                        method, regexPath, address,
                        maxCount, duration, unit.name()));

                    // Save the IP into the log/request index
                    if (logRejectedRequests) {
                        if (!trace) log.warn(String.format("Reject %s request [%s] from address {%s} - maxCallCount: %s, duration: '%s%s'",
                            method, regexPath, address,
                            maxCount, duration, unit.name()));

                        RequestLog request = new RequestLog();
                        request.setHost(address);
                        request.setMethod(method.name());
                        request.setPath(regexPath);
                        requestLogService.save(request);
                    }
                };
            QuotaMap.RemovableListener removeLimitationListener = !trace ? null : (removalNotification) -> {
                log.trace(String.format("Forget quota limitation on %s request [%s] from address {%s} - %s",
                    method, regexPath,
                    removalNotification.getKey(),
                    removalNotification.getCause().name()
                ));
            };

            quotaByRequest.put(regexPath, new QuotaMap(regexPath,
                maxCount, duration, unit,
                onMaxExceededListener, removeLimitationListener));
        }
        else {
            log.warn(String.format("More than one quota defined for request %s (%s). Skipping new quota config", regexPath, method.toString()));
        }

        return this;
    }

    public boolean isAllow(RestRequest request) {
        if (!this.enable) return true;

        RestRequest.Method method = request.method();
        String path = request.path();

        // Is path cacheable ?
        boolean cacheable = CACHE_PATH_REGEXP.matcher(path).matches();

        if (cacheable) {
            Set<String> noQuotaPaths = noQuotasCache.get(method);
            if (noQuotaPaths != null && noQuotaPaths.contains(path)) {
                if (trace) log.trace(String.format("No matching quota for %s request [%s]: allow (cached)", method, path));
                return true; // Always allow (no quota)
            }
        }

        // Get the IP
        String ip = RestUtils.getIPAddress(request);

        if (ip != null) {
            // Check if whitelisted
            if (ipWhiteList.contains(ip)) {
                if (trace)
                    log.trace(String.format("Checking quota for %s request [%s]: OK (address {%s} is whitelisted)", method, path, ip));
                return true;
            }
            // Check if blacklisted
            if (ipBlackList.contains(ip)) {
                if (trace)
                    log.trace(String.format("Checking quota for %s request [%s]: KO (address {%s} is blacklisted)", method, path, ip));
                return false;
            }
        }

        if (trace) log.trace(String.format("Checking quota for %s request [%s]...", method, path));
        Map<String, QuotaMap> quotas = quotasByMethod.get(request.method());

        if (quotas == null) {
            if (trace) log.trace(String.format("No matching quota defined for %s request [%s]: continue", method, path));
            return true;
        }
        boolean found = false;
        for (String pathRegexp : quotas.keySet()) {
            if (trace) log.trace(String.format(" - Trying against quota [%s] for %s requests", pathRegexp, method));

            // A quota exists for this path
            if (path.matches(pathRegexp)) {
                if (trace) log.trace(String.format("Find matching quota [%s] for %s request [%s]", pathRegexp, method, path));

                // NO IP not allow, because we cannot check
                if (StringUtils.isEmpty(ip)) {
                    if (trace) log.trace(String.format("No IP address found in request: reject", pathRegexp, method, path));
                    return false;
                }

                QuotaMap quota = quotas.get(pathRegexp);

                // If cannot increment: NOT allow
                if (!quota.increment(ip)) {
                    if (trace) log.trace(String.format("Reject %s request [%s] - Too many requests from address {%s}", method, path, ip));
                    return false;
                }

                found = true;
                break;
            }
        }
        if (!found) {
            if (trace) log.trace(String.format("No matching quota for %s request [%s]: allow", method, path));

            // If cacheable path: remember no quota should be applied
            if (cacheable) {
                synchronized (noQuotasCache) {
                    noQuotasCache
                        .computeIfAbsent(method, k -> new TreeSet<>())
                        .add(path);
                }
            }
        }
        return true;
    }

    @Override
    protected void doStart() {

    }

    @Override
    protected void doStop() {

    }

    @Override
    protected void doClose() {

    }

}
