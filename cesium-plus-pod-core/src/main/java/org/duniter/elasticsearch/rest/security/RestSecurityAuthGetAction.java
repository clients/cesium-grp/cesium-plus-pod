package org.duniter.elasticsearch.rest.security;

/*
 * #%L
 * duniter4j-elasticsearch-plugin
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.duniter.core.exception.TechnicalException;
import org.duniter.elasticsearch.rest.JacksonJsonRestResponse;
import org.duniter.elasticsearch.security.challenge.AuthToken;
import org.duniter.elasticsearch.security.challenge.ChallengeMessageStore;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.rest.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.rest.RestRequest.Method.GET;

public class RestSecurityAuthGetAction extends BaseRestHandler {

    private ChallengeMessageStore challengeMessageStore;

    @Inject
    public RestSecurityAuthGetAction(Settings settings, RestController controller,
                                     Client client,
                                     RestSecurityController securityController,
                                     ChallengeMessageStore challengeMessageStore,
                                     RestQuotaController quotaController
                                          ) {
        super(settings, controller, client);
        this.challengeMessageStore = challengeMessageStore;
        controller.registerHandler(GET, "/auth", this);
        securityController.allow(GET, "/auth");
        quotaController.quota(GET, "/auth", 10, 1, TimeUnit.HOURS); // 10 auth by hour
    }

    @Override
    protected void handleRequest(final RestRequest request, RestChannel channel, Client client) throws Exception {
        AuthToken challenge = challengeMessageStore.createNewChallenge();
        try {
            channel.sendResponse(new JacksonJsonRestResponse(request, RestStatus.OK, challenge));
        }
        catch(IOException ioe) {
            throw new TechnicalException(String.format("Error while generating JSON for [/auth]: %s", ioe.getMessage()), ioe);
        }
    }

}
