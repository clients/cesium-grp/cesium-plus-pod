package org.duniter.elasticsearch.rest.cluster;

/*-
 * #%L
 * Cesium+ pod :: Core plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.duniter.elasticsearch.dao.RequestLogRepository;
import org.duniter.elasticsearch.rest.security.RestSecurityController;
import org.duniter.elasticsearch.security.token.SecurityTokenStore;
import org.duniter.elasticsearch.util.RestUtils;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.logging.Loggers;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.rest.BytesRestResponse;
import org.elasticsearch.rest.RestChannel;
import org.elasticsearch.rest.RestController;
import org.elasticsearch.rest.RestRequest;

import static org.elasticsearch.rest.RestRequest.Method.GET;
import static org.elasticsearch.rest.RestStatus.FORBIDDEN;
import static org.elasticsearch.rest.RestStatus.UNAUTHORIZED;

/**
 *
 */
public class RestClusterHealthAction extends org.elasticsearch.rest.action.admin.cluster.health.RestClusterHealthAction {

    private final ESLogger log;

    private final SecurityTokenStore securityTokenStore;

    @Inject
    public RestClusterHealthAction(Settings settings, RestController controller, Client client,
                                   RestSecurityController securityController,
                                   SecurityTokenStore securityTokenStore) {
        super(settings, controller, client);
        log = Loggers.getLogger("duniter.rest." + RequestLogRepository.INDEX, settings, String.format("[%s]", RequestLogRepository.INDEX));
        this.securityTokenStore = securityTokenStore;
        securityController.allow(GET, "/_cluster/health");
        securityController.allow(GET, "/_cluster/health/[^/]+");
        controller.registerHandler(GET, "/_cluster/health", this);
        controller.registerHandler(GET, "/_cluster/health/{index}", this);
    }

    @Override
    public void handleRequest(final RestRequest request, final RestChannel channel, final Client client) {

        // Check validation token
        String authorization = request.getHeader("Authorization");
        if (authorization == null || authorization.startsWith("token: ")) {
            channel.sendResponse(new BytesRestResponse(UNAUTHORIZED));
            return;
        }

        String token = authorization.substring(authorization.indexOf("token: ".length())).trim();

        if (!securityTokenStore.validateToken(token)) {

            String ip = RestUtils.getIPAddress(request);
            log.warn(String.format("Reject request to [_cluster/health] from {%s} - Invalid token: %s",
                ip, token));

            channel.sendResponse(new BytesRestResponse(FORBIDDEN));
            return;
        }

        log.debug("Authorized access to [_cluster/health]");

        super.handleRequest(request, channel, client);
    }
}
