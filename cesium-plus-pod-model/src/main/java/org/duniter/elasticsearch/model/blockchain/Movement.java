package org.duniter.elasticsearch.model.blockchain;

/*
 * #%L
 * Duniter4j :: Core Client API
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import org.duniter.core.client.model.bma.BlockchainBlock;
import org.duniter.core.model.IEntity;
import org.duniter.core.util.Preconditions;
import org.duniter.elasticsearch.dao.blockchain.IBlockRepository;

import java.io.Serializable;

/**
 * Created by blavenie on 29/11/16.
 */
@Data
@FieldNameConstants
public class Movement implements IEntity<String>, Serializable {


    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(BlockchainBlock block) {
        return new Builder(block);
    }


    // ES identifier
    private String id;

    // Property copied from Block
    private String currency;
    private Long medianTime;

    // Property copied from Tx
    private int version;
    private String issuer;
    private String recipient;
    private Long amount;
    private Integer unitbase;
    private String comment;

    // Specific properties
    private boolean ud;
    private Reference reference;

    public Movement() {
        super();
    }

    @Override
    @JsonIgnore
    public String getId() {
        return id;
    }

    @Override
    @JsonIgnore
    public void setId(String id) {
        this.id = id;
    }

    public static class Builder {

        private Movement result;

        private Builder() {
            result = new Movement();
        }

        public Builder(BlockchainBlock block) {
            this();
            setBlock(block);
        }

        public Builder setBlock(BlockchainBlock block) {
            result.setCurrency(block.getCurrency());
            result.setMedianTime(block.getMedianTime());
            result.setReference(Reference.builder()
                    .index(block.getCurrency())
                    .type(IBlockRepository.TYPE)
                    .id(String.valueOf(block.getNumber())).build());
            setReferenceHash(block.getHash());
            return this;
        }

        public Builder setReferenceHash(String hash) {
            Preconditions.checkNotNull(result.getReference(), "No reference set. Please call setReference() first");
            result.getReference().setHash(hash);
            return this;
        }

        public Builder setRecipient(String recipient) {
            result.setRecipient(recipient);
            return this;
        }

        public Builder setIssuer(String issuer) {
            result.setIssuer(issuer);
            return this;
        }

        public Builder setVersion(int version) {
            result.setVersion(version);
            return this;
        }

        public Builder setComment(String comment) {
            result.setComment(comment);
            return this;
        }

        public Builder setAmount(long amount, int unitbase) {
            result.setAmount(amount);
            result.setUnitbase(unitbase);
            return this;
        }

        public Builder setUD(boolean ud) {
            result.setUd(ud);
            return this;
        }

        public Movement build() {
            Preconditions.checkNotNull(result);
            Preconditions.checkNotNull(result.getAmount());
            Preconditions.checkNotNull(result.getUnitbase());
            Preconditions.checkNotNull(result.getRecipient());
            Preconditions.checkNotNull(result.getIssuer());
            Preconditions.checkNotNull(result.getCurrency());

            return result;
        }
    }

    @Data
    @lombok.Builder
    @FieldNameConstants
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Reference {

        private String index;

        private String type;

        private String id;

        private String anchor;

        private String hash;

        public Reference(String index, String type, String id) {
            this(index, type, id, null);
        }

        public Reference(String index, String type, String id, String anchor) {
            this.index = index;
            this.type = type;
            this.id = id;
            this.anchor = anchor;
        }

        public Reference(Reference another) {
            this.index = another.getIndex();
            this.type = another.getType();
            this.id = another.getId();
            this.hash = another.getHash();
            this.anchor = another.getAnchor();
        }

    }
}
