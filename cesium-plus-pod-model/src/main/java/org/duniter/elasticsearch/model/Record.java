package org.duniter.elasticsearch.model;

/*
 * #%L
 * Duniter4j :: Core Client API
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import org.duniter.core.model.IEntity;

/**
 * Created by blavenie on 01/03/16.
 */
@Data
@FieldNameConstants
public class Record implements IEntity<String> {

    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Fields {}

    private Integer version;
    private String id;
    private String issuer;
    private String hash;
    private String signature;
    private Long time;

    public Record() {
    }

    public Record(Record another) {
        this.version = another.getVersion();
        this.id = another.getId();
        this.issuer = another.getIssuer();
        this.hash = another.getHash();
        this.signature = another.getSignature();
        this.time = another.getTime();
    }
}
