package org.duniter.elasticsearch.model.query;

/*-
 * #%L
 * Cesium+ pod :: Model
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.JsonNode;

public class SearchHit {

    protected String id;

    protected JsonNode source;

    public SearchHit() {
    }

    @JsonGetter("_id")
    public String getId() {
        return id;
    }

    @JsonSetter("_id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonGetter("_source")
    public JsonNode getSource() {
        return source;
    }

    @JsonSetter("_source")
    public void setSource(JsonNode source) {
        this.source = source;
    }

}
