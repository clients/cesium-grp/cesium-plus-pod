package org.duniter.elasticsearch.model.subscription;

/*
 * #%L
 * Duniter4j :: ElasticSearch GChange plugin
 * %%
 * Copyright (C) 2014 - 2017 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import org.duniter.elasticsearch.model.Record;

/**
 * Created by blavenie on 01/12/16.
 */
@Data
@FieldNameConstants
public class SubscriptionRecord<T> extends Record{

    public static class Fields extends Record.Fields {}

    private String recipient;
    private String nonce;
    private String recipientContent;
    private String issuerContent;
    private String type;
    private T content;

    @JsonIgnore
    public T getContent() {
        return content;
    }

    @JsonIgnore
    public void setContent(T content) {
        this.content = content;
    }
}
