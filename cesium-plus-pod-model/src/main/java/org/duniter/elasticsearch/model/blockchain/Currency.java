package org.duniter.elasticsearch.model.blockchain;

/*
 * #%L
 * Duniter4j :: Core API
 * %%
 * Copyright (C) 2014 - 2015 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

/**
 * Created by eis on 05/02/15.
 */

@Data
@FieldNameConstants
public class Currency extends org.duniter.core.client.model.local.Currency {

    public static class Fields extends org.duniter.core.client.model.local.Currency.Fields {}

    public interface JsonFields {
        @Deprecated
        String LAST_UD = "lastUD";
    }

    private String[] tags;
    private String issuer;

    /**
     * @deprecated use id
     * @return
     */
    @Deprecated
    public String getCurrency() {
        return getId();
    }

    /**
     * @deprecated use id
     * @return
     */
    @Deprecated
    public void setCurrency(String currency) {
        this.setId(currency);
    }

    @JsonAlias("lastUD")
    public Long getDividend() {
        return super.getDividend();
    }

    @JsonAlias("lastUD")
    public void setDividend(Long dividend) {
        super.setDividend(dividend);
    }

}
