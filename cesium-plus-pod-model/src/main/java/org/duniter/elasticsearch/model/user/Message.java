package org.duniter.elasticsearch.model.user;

/*
 * #%L
 * Duniter4j :: Core Client API
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import org.duniter.elasticsearch.model.Record;
import org.duniter.core.exception.TechnicalException;

/**
 * Created by blavenie on 29/11/16.
 */
@Data
@FieldNameConstants
public class Message extends Record {

    public static class Fields extends Record.Fields {}

    public interface JsonFields {
        String READ_SIGNATURE="read_signature";
    }

    private String nonce;
    private String recipient;
    private String title;
    private String content;
    private String readSignature;

    @JsonGetter(JsonFields.READ_SIGNATURE)
    public String getReadSignature() {
        return readSignature;
    }

    @JsonSetter(JsonFields.READ_SIGNATURE)
    public void setReadSignature(String readSignature) {
        this.readSignature = readSignature;
    }

    @JsonIgnore
    public String toJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            return mapper.writeValueAsString(this);
        } catch(Exception e) {
            throw new TechnicalException(e);
        }
    }

}
