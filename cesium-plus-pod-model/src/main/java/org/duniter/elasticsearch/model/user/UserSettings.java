package org.duniter.elasticsearch.model.user;

/*
 * #%L
 * Duniter4j :: Core Client API
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import org.duniter.elasticsearch.model.Record;
import org.duniter.elasticsearch.model.type.Attachment;
import org.duniter.elasticsearch.model.type.GeoPoint;

/**
 * Created by blavenie on 01/03/16.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class UserSettings extends Record {
    public static class Fields extends Record.Fields {}

    private String nonce;
    private String content;

    @Override
    @JsonIgnore
    public String getId() {
        return super.getIssuer();
    }

    @Override
    @JsonIgnore
    public void setId(String id) {
        super.setIssuer(id);
    }

}
