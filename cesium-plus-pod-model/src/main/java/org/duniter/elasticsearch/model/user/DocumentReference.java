package org.duniter.elasticsearch.model.user;

/*-
 * #%L
 * Cesium+ pod :: Model
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public class DocumentReference {

    public interface Fields {
        String INDEX="index";
        String TYPE="type";
        String ID="id";
        String ANCHOR="anchor";
        String HASH="hash";
    }

    private String index;

    private String type;

    private String id;

    private String anchor;

    private String hash;

    public DocumentReference() {
    }

    public DocumentReference(String index, String type, String id) {
        this(index, type, id, null);
    }

    public DocumentReference(String index, String type, String id, String anchor) {
        this.index = index;
        this.type = type;
        this.id = id;
        this.anchor = anchor;
    }

    public DocumentReference(DocumentReference another) {
        this.index = another.getIndex();
        this.type = another.getType();
        this.id = another.getId();
        this.hash = another.getHash();
        this.anchor = another.getAnchor();
    }

    public String getIndex() {
        return index;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getAnchor() {
        return anchor;
    }

    public void setAnchor(String anchor) {
        this.anchor = anchor;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
