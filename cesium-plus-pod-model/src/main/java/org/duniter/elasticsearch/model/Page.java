package org.duniter.elasticsearch.model;

/*-
 * #%L
 * Cesium+ pod :: Model
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Page {

    public static final Page DEFAULT = Page.builder().build();

    public static Page nullToDefault(Page page) {
            return page != null ? page : DEFAULT;
    }
    @Builder.Default
    private Integer from = 0;

    @Builder.Default
    private Integer size = 1000;

    @Builder.Default
    private String sortBy = null;

    @Builder.Default
    private SortDirection sortDirection = SortDirection.ASC;

}
