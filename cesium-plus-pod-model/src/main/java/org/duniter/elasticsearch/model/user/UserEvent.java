package org.duniter.elasticsearch.model.user;

/*
 * #%L
 * Duniter4j :: Core Client API
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import org.duniter.core.util.Preconditions;
import org.duniter.elasticsearch.model.Record;
import org.nuiton.i18n.I18n;

import java.util.Locale;

/**
 * Created by blavenie on 29/11/16.
 */
@Data
@FieldNameConstants
public class UserEvent extends Record {

    public static class Fields extends Record.Fields {}
    public interface JsonFields {
        String READ_SIGNATURE="read_signature";
    }

    public enum EventType {
        INFO,
        WARN,
        ERROR
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(UserEvent.EventType type, String code) {
        return new Builder(type, code, null, null);
    }

    public static Builder builder(UserEvent event) {
        return new Builder(event);
    }

    public static Builder builder(UserEvent.EventType type, String code, String message, String... params) {
        return new Builder(type, code, message, params);
    }


    private String id;

    private EventType type;

    private String recipient;

    private String code;

    private String message;

    private String[] params;

    private DocumentReference reference;

    private String readSignature;

    public UserEvent() {
        super();
    }

    public UserEvent(EventType type, String code, String message, String... params) {
        super();
        this.type = type;
        this.code = code;
        this.message = message;
        this.params = params;
        setTime(getDefaultTime());
    }

    public UserEvent(UserEvent another) {
        super(another);
        this.type = another.getType();
        this.code = another.getCode();
        this.params = another.getParams();
        this.reference = (another.getReference() != null) ? new DocumentReference(another.getReference()) : null;
        this.message = another.getMessage();
        this.recipient = another.getRecipient();
        this.readSignature = another.getReadSignature();
    }

    @JsonIgnore
    public String getLocalizedMessage(Locale locale) {
        return I18n.l(locale, this.message, this.params);
    }

    @JsonGetter(JsonFields.READ_SIGNATURE)
    public String getReadSignature() {
        return readSignature;
    }

    @JsonSetter(JsonFields.READ_SIGNATURE)
    public void setReadSignature(String readSignature) {
        this.readSignature = readSignature;
    }

    private static long getDefaultTime() {
        return Math.round(1d * System.currentTimeMillis() / 1000);
    }

    public static class Builder {

        private UserEvent result;

        private Builder() {
            result = new UserEvent();
        }

        public Builder(UserEvent.EventType type, String code, String message, String... params) {
            result = new UserEvent(type, code, message, params);
        }

        public Builder(UserEvent event) {
            result = new UserEvent(event.getType(), event.getCode(), event.getMessage(), event.getParams());
            if (event.getReference() != null) {
                setReference(event.getReference().getIndex(), event.getReference().getType(), event.getReference().getId());
                setReferenceAnchor(event.getReference().getAnchor());
                setReferenceHash(event.getReference().getHash());
            }
            setRecipient(event.getRecipient());
            setIssuer(event.getIssuer());
            setTime(event.getTime());
        }

        public Builder setMessage(String message, String... params) {
            result.setMessage(message);
            result.setParams(params);
            return this;
        }

        public Builder setRecipient(String recipient) {
            result.setRecipient(recipient);
            return this;
        }

        public Builder setIssuer(String issuer) {
            result.setIssuer(issuer);
            return this;
        }

        public Builder setReference(String index, String type, String id) {
            result.setReference(new DocumentReference(index, type, id));
            return this;
        }

        public Builder setReferenceHash(String hash) {
            Preconditions.checkNotNull(result.getReference(), "No reference set. Please call setReference() first");
            result.getReference().setHash(hash);
            return this;
        }

        public Builder setReference(String index, String type, String id, String anchor) {
            result.setReference(new DocumentReference(index, type, id, anchor));
            return this;
        }

        public Builder setReferenceAnchor(String anchor) {
            Preconditions.checkNotNull(result.getReference(), "No reference set. Please call setReference() first");
            result.getReference().setAnchor(anchor);
            return this;
        }

        public Builder setTime(long time) {
            result.setTime(time);
            return this;
        }

        public Builder setType(EventType type) {
            result.setType(type);
            return this;
        }

        public UserEvent build() {
            if (result.getTime() == null) {
                result.setTime(getDefaultTime());
            }
            return new UserEvent(result);
        }
    }


}
