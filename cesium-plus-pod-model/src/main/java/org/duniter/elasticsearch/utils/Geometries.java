package org.duniter.elasticsearch.utils;

/*-
 * #%L
 * Cesium+ pod :: Model
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.NonNull;
import org.duniter.elasticsearch.model.type.GeoPoint;
import org.geojson.LngLatAlt;
import org.geojson.Point;
import org.geojson.Polygon;

import java.util.regex.Pattern;

public class Geometries {

    public interface SRID {
        Integer NONE = 0;
        Integer WGS86 = 4326;
    }

    protected static final double EARTH_RADIUS = 6378288.0;
    protected static Pattern COORDINATES_PATTERN = Pattern.compile("([0-9]{4})([NS])[\\s\\xA0]+([0-9]{5})([EW])[\\s\\xA0]*");
    protected final static String WKT_POINT = "POINT(%s %s)";
    protected final static String WKT_MULTIPOINT_BASE = "MULTIPOINT(%s)";
    protected final static String WKT_MULTIPOINT_SUBPART = "(%s %s)";
    protected final static String WKT_POLYGON = "POLYGON((%s))";
    protected final static String WKT_POLYGON_SUBPART = "%s %s";
    protected final static String WKT_MULTIPOLYGON = "MULTIPOLYGON((%s))";
    protected final static String WKT_MULTIPOLYGON_SUBPART = "(%s)";

    /**
     * <p>createPoint.</p>
     *
     * @param x a {@link Double} object.
     * @param y a {@link Double} object.
     * @return a {@link Point} object.
     */
    public static Point createPoint(double x, double y) {
        return new Point(x, y);
    }

    /**
     * <p>createPoint.</p>
     *
     * @param x a {@link Double} object.
     * @param y a {@link Double} object.
     * @return a {@link Point} object.
     */
    public static LngLatAlt createCoordinate(double x, double y) {
        return new LngLatAlt(x, y);
    }

    /**
     * <p>createPoint.</p>
     *
     * @param x a {@link Double} object.
     * @param y a {@link Double} object.
     * @return a {@link Point} object.
     */
    public static Point toGeometry(GeoPoint point) {
        return new Point(point.getLon(), point.getLat());
    }

    /**
     * <p>createPolygon.</p>
     *
     * @param minX a {@link Double} object.
     * @param minY a {@link Double} object.
     * @param maxX a {@link Double} object.
     * @param maxY a {@link Double} object.
     * @return a {@link Polygon} object.
     */
    public static Polygon createPolygon(Double minX, Double minY, Double maxX, Double maxY) {
        return new Polygon(
            new LngLatAlt(minX, minY),
            new LngLatAlt(minX, maxY),
            new LngLatAlt(maxX, maxY),
            new LngLatAlt(maxX, minY),
            new LngLatAlt(minX, minY));
    }

    /**
     * <p>getDistanceInMeters.</p>
     *
     * @param x1 a {@link Number} object.
     * @param y1 a {@link Number} object.
     * @param x2 a {@link Number} object.
     * @param y2 a {@link Number} object.
     * @return a int.
     */
    public static int getDistanceInMeters(@NonNull GeoPoint p1,
                                          @NonNull GeoPoint p2) {
        return getDistanceInMeters(p1.getLon(), p1.getLat(), p2.getLon(), p2.getLat());
    }
    /**
     * <p>getDistanceInMeters.</p>
     *
     * @param x1 a {@link Number} object.
     * @param y1 a {@link Number} object.
     * @param x2 a {@link Number} object.
     * @param y2 a {@link Number} object.
     * @return a int.
     */
    public static int getDistanceInMeters(@NonNull Number x1, @NonNull Number y1,
                                          @NonNull Number x2, @NonNull Number y2) {

        double sLat = y1.doubleValue() * Math.PI / 180.0;
        double sLong = x1.doubleValue() * Math.PI / 180.0;
        double eLat = y2.doubleValue() * Math.PI / 180.0;
        double eLong = x2.doubleValue() * Math.PI / 180.0;

        Double d = EARTH_RADIUS *
            (Math.PI / 2 - Math.asin(Math.sin(eLat) * Math.sin(sLat)
                + Math.cos(eLong - sLong) * Math.cos(eLat) * Math.cos(sLat)));
        return d.intValue();
    }

    /**
     * <p>getDistanceInMilles.</p>
     *
     * @param distance a {@link Float} object.
     * @return a {@link String} object.
     */
    public static String getDistanceInMilles(Number distance) {
        String distanceText;
        if (distance != null) {
            double distanceInMilles = distance.doubleValue() / 1852;
            distanceText = String.format("%.3d", distanceInMilles);

        } else {
            distanceText = "";
        }
        return distanceText;
    }
}
