text_email(title, issuerPubkey, issuerName, senderPubkey, senderName, events, showListDivider, url, linkName, bgColor, logoUrl, showOpenButton, locale) ::= <<
$i18n_args("duniter4j.es.subscription.email.hello", issuerName)$
$if(showListDivider)$$i18n_args("duniter4j.es.subscription.email.unreadCount", {$length(events)$} )$$endif$

$if(showListDivider)$$i18n("duniter4j.es.subscription.email.notificationsDivider")$$endif$
$events:{e|$text_event_item(e)$}$

$if(showOpenButton)$$i18n("duniter4j.es.subscription.email.open")$ $linkName$ : $url$$endif$
$if(issuerPubkey)$$i18n_args("duniter4j.es.subscription.email.pubkey", [{$[url, "/#/app/wot/", issuerPubkey, "/"]; separator=""$}, {$issuerPubkey; format="pubkey"$}, linkName])$$endif$

-----------------------------------------------
$i18n_args("duniter4j.es.subscription.email.footer.sendBy", [{$[url, "/#/app/wot/", senderPubkey, "/"]; separator=""$}, senderName, linkName])$
$i18n_args("duniter4j.es.subscription.email.footer.disableHelp", [{$[url, "/#/app/wallet/subscriptions"]; separator=""$}, linkName])$
>>