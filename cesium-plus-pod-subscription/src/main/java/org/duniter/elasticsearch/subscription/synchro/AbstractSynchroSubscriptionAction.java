package org.duniter.elasticsearch.subscription.synchro;

/*-
 * #%L
 * Cesium+ pod :: Subscription plugin
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.duniter.core.client.model.bma.EndpointApi;
import org.duniter.core.service.CryptoService;
import org.duniter.elasticsearch.subscription.PluginSettings;
import org.duniter.elasticsearch.client.Duniter4jClient;
import org.duniter.elasticsearch.synchro.AbstractSynchroAction;
import org.duniter.elasticsearch.threadpool.ThreadPool;

public abstract class AbstractSynchroSubscriptionAction extends AbstractSynchroAction {

    private String endpointApi;

    public AbstractSynchroSubscriptionAction(String index, String type,
                                             Duniter4jClient client,
                                             PluginSettings pluginSettings,
                                             CryptoService cryptoService,
                                             ThreadPool threadPool) {
        super(index, type, index, type, client, pluginSettings.getDelegate().getDelegate(), cryptoService, threadPool);

        // Define endpoint API to used by synchronization, to select peers to request
        this.endpointApi = pluginSettings.getSubscriptionEndpointApi();

    }

    public AbstractSynchroSubscriptionAction(String fromIndex, String fromType,
                                             String toIndex, String toType,
                                             Duniter4jClient client,
                                             PluginSettings pluginSettings,
                                             CryptoService cryptoService,
                                             ThreadPool threadPool) {
        super(fromIndex, fromType, toIndex, toType, client, pluginSettings.getDelegate().getDelegate(), cryptoService, threadPool);

        // Define endpoint API to used by synchronization, to select peers to request
        this.endpointApi = pluginSettings.getSubscriptionEndpointApi();
    }

    @Override
    public String getEndPointApi() {
        return endpointApi;
    }
}
