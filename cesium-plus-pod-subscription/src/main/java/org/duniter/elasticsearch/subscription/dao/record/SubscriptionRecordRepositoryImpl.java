package org.duniter.elasticsearch.subscription.dao.record;

/*-
 * #%L
 * Duniter4j :: ElasticSearch Subscription plugin
 * %%
 * Copyright (C) 2014 - 2017 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.duniter.core.exception.TechnicalException;
import org.duniter.core.util.Beans;
import org.duniter.core.util.CollectionUtils;
import org.duniter.core.util.Preconditions;
import org.duniter.core.util.StringUtils;
import org.duniter.elasticsearch.subscription.PluginSettings;
import org.duniter.elasticsearch.subscription.dao.AbstractSubscriptionIndexTypeRepository;
import org.duniter.elasticsearch.subscription.dao.SubscriptionIndexRepository;
import org.duniter.elasticsearch.model.subscription.SubscriptionRecord;
import org.duniter.elasticsearch.model.subscription.email.EmailSubscription;
import org.duniter.elasticsearch.model.Page;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by blavenie on 03/04/17.
 */
public class SubscriptionRecordRepositoryImpl extends AbstractSubscriptionIndexTypeRepository<SubscriptionRecordRepositoryImpl> implements SubscriptionRecordRepository<SubscriptionRecordRepositoryImpl> {

    @Inject
    public SubscriptionRecordRepositoryImpl(PluginSettings pluginSettings, SubscriptionIndexRepository indexDao) {
        super(SubscriptionIndexRepository.INDEX, TYPE, pluginSettings);

        indexDao.register(this);
    }

    @Override
    public<T extends SubscriptionRecord<?>> List<T> findAllByRecipient(String recipient, Class<? extends T> clazz, @Nullable Page page, String... types) {
        Preconditions.checkArgument(StringUtils.isNotBlank(recipient));

        BoolQueryBuilder query = QueryBuilders.boolQuery()
                .filter(QueryBuilders.termQuery(SubscriptionRecord.Fields.RECIPIENT, recipient));
        if (CollectionUtils.isNotEmpty(types)) {
            query.filter(QueryBuilders.termsQuery(SubscriptionRecord.Fields.TYPE, types));
        }

        return findAll(QueryBuilders.constantScoreQuery(query), clazz, page);
    }

    @Override
    public <T extends SubscriptionRecord<?>> List<T> findAll(QueryBuilder query, Class<? extends T> clazz, Page page) {
        Preconditions.checkNotNull(clazz);

        SearchRequestBuilder queryBuilder = client.prepareSearch(SubscriptionIndexRepository.INDEX)
            .setTypes(SubscriptionRecordRepository.TYPE)
            .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
            .setQuery(query)
            .setFetchSource(true);

        if (page != null) {
            if (page.getFrom() != null) queryBuilder.setFrom(page.getFrom());
            if (page.getSize() != null) queryBuilder.setSize(page.getSize());
        }

        return Arrays.stream(queryBuilder.get().getHits().getHits())
            .map(hit -> this.toSubscription(hit, clazz))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    @Override
    public XContentBuilder createTypeMapping() {
        try {
            XContentBuilder mapping = XContentFactory.jsonBuilder().startObject()
                    .startObject(getType())
                    .startObject("properties")

                    // version
                    .startObject(SubscriptionRecord.Fields.VERSION)
                    .field("type", "integer")
                    .endObject()

                    // type
                    .startObject(SubscriptionRecord.Fields.TYPE)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // issuer
                    .startObject(SubscriptionRecord.Fields.ISSUER)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // recipient
                    .startObject(SubscriptionRecord.Fields.RECIPIENT)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // time
                    .startObject(SubscriptionRecord.Fields.TIME)
                    .field("type", "integer")
                    .endObject()

                    // nonce
                    .startObject(SubscriptionRecord.Fields.NONCE)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // issuerContent
                    .startObject(SubscriptionRecord.Fields.ISSUER_CONTENT)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // receiver content
                    .startObject(SubscriptionRecord.Fields.RECIPIENT_CONTENT)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // hash
                    .startObject(SubscriptionRecord.Fields.HASH)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // signature
                    .startObject(SubscriptionRecord.Fields.SIGNATURE)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    .endObject()
                    .endObject().endObject();

            return mapping;
        }
        catch(IOException ioe) {
            throw new TechnicalException(String.format("Error while getting mapping for index [%s/%s]: %s", getIndex(), getType(), ioe.getMessage()), ioe);
        }
    }

    protected <T extends SubscriptionRecord<?>> T toSubscription(SearchHit searchHit, Class<? extends T> clazz) {

        T record = client.readSourceOrNull(searchHit, clazz);
        record.setId(searchHit.getId());

        return record;
    }

}
