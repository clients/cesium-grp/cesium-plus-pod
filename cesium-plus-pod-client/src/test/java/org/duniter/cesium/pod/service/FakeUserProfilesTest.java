package org.duniter.cesium.pod.service;

/*-
 * #%L
 * Cesium+ pod :: Client API
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.duniter.cesium.pod.TestResource;
import org.duniter.core.client.model.bma.jackson.JacksonUtils;
import org.duniter.core.client.service.exception.HttpNotFoundException;
import org.duniter.core.client.service.exception.HttpTimeoutException;
import org.duniter.core.client.service.exception.HttpUnauthorizeException;
import org.duniter.core.exception.TechnicalException;
import org.duniter.core.util.ArrayUtils;
import org.duniter.core.util.Beans;
import org.duniter.core.util.CollectionUtils;
import org.duniter.core.util.StringUtils;
import org.duniter.elasticsearch.model.Page;
import org.duniter.elasticsearch.model.SortDirection;
import org.duniter.elasticsearch.client.model.filter.MovementFilter;
import org.duniter.elasticsearch.client.model.filter.UserProfileFilter;
import org.duniter.elasticsearch.client.model.filter.UserSettingsFilter;
import org.duniter.elasticsearch.client.model.geom.Envelope;
import org.duniter.elasticsearch.client.service.ServiceLocator;
import org.duniter.elasticsearch.client.service.UserProfileService;
import org.duniter.elasticsearch.client.service.UserSettingsService;
import org.duniter.elasticsearch.model.blockchain.Movement;
import org.duniter.elasticsearch.model.type.Attachment;
import org.duniter.elasticsearch.model.type.GeoPoint;
import org.duniter.elasticsearch.model.user.UserProfile;
import org.duniter.elasticsearch.model.user.UserSettings;
import org.duniter.elasticsearch.utils.Dates;
import org.duniter.elasticsearch.utils.Geometries;
import org.junit.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class FakeUserProfilesTest extends AbstractServiceTest<UserProfileService>{

    @ClassRule
    public static final TestResource resource = TestResource.create();

    private UserSettingsService settingsService;

    public FakeUserProfilesTest(){
        super(UserProfileService.class);
    }

    @Before
    public void setUp() {
        super.setUp();
        settingsService = ServiceLocator.instance().getUserSettingsService();
        peer = getPeer(resource.getFixtures().getDefaultCurrency());
        wallet = getWallet(resource.getFixtures().getDefaultCurrency(),
            null,
            resource.getFixtures().getUserPublicKey(),
            resource.getFixtures().getUserSecretKey()
            );
    }

    @Test
    public void deleteDicoProfile() throws IOException, ParseException {

        // Load dictionary, from fake profiles
        Set<String> dictionary = createDictionary(ImmutableList.<UserProfile>builder()
            .addAll(loadProfilesFile(new File("src/test/resources/fake-profiles-543.json")))
            .addAll(loadProfilesFile(new File("src/test/resources/fake-profiles-4600.json")))
            .addAll(loadProfilesFile(new File("src/test/resources/fake-profiles-G.json")))
            .build());

        List<UserProfile> profiles;
        List<UserProfile> rejectedProfiles = Lists.newArrayList();

        String filePathPattern = "target/similar-profiles-%s.json";
        File inputProfilesFile = new File(String.format(filePathPattern, "input"));

        if (inputProfilesFile.exists()) {
            profiles = loadProfilesFile(inputProfilesFile);
        }
        else {
            profiles = Lists.newArrayList();
            Date startDate = Dates.parseDate("2021-06-30", "yyy-MM-dd");
            long now = System.currentTimeMillis();
            long sliceDuration = 2 * 30 * 24 * 60 * 60 * 1000; // 2 months, in millis

            for (long startTime = startDate.getTime(); startTime < now; startTime += sliceDuration) {
                long endTime = Math.min(now, startTime + sliceDuration);
                profiles.addAll(loadProfiles(UserProfileFilter.builder()
                    .startDate(new Date(startTime))
                    .endDate(new Date(endTime))
                    .build()));
            }

            log.info("Found {} profiles", profiles.size());

            // Filter no settings
            profiles = filterNoSettings(profiles, rejectedProfiles);

            // Filter no Tx
            profiles = filterNoTx(profiles, rejectedProfiles);

            dumpToFile(profiles, inputProfilesFile, true);
            dumpToGeoJsonFile(profiles, new File(inputProfilesFile.getPath() + ".geojson"), true);

        }

        // Force dictionary profile
        //profiles = filterDictionary(profiles, dictionary);

        // Filter no description
        //profiles = profiles.stream().filter(p -> StringUtils.isBlank(p.getDescription())).collect(Collectors.toList());

        // Filter no address
        //profiles = profiles.stream().filter(p -> StringUtils.isBlank(p.getAddress())).collect(Collectors.toList());

        // Filter no postal code
        //profiles = profiles.stream().filter(p -> StringUtils.isNotBlank(p.getCity())
        //    && !p.getCity().contains("0") && !p.getCity().contains("1"))
        //    .collect(Collectors.toList());

        // Filter no geo point
        //profiles = profiles.stream().filter(p -> p.getGeoPoint() == null)
        //    .collect(Collectors.toList());

        //dumpToGeoJsonFile(profiles, new File(inputProfilesFile.getPath() + ".geojson"), true);
        //dumpToFile(profiles, inputProfilesFile, true);

        double startDetectionThreshold = 70;
        double thresholdStep = 10;
        double minGroupSize = 20;

        Map<Integer, Integer> profileCountByHash = Maps.newLinkedHashMap();
        Map<Integer, File> fileByHash = Maps.newLinkedHashMap();

        List<List<UserProfile>> inputs = Lists.newArrayList();
        inputs.add(profiles);

        int groupIndex = 0;
        while (!inputs.isEmpty()) {
            List<UserProfile> loopInput = inputs.remove(0);
            double detectionThreshold = startDetectionThreshold;
            while (detectionThreshold < 100) {

                List<UserProfile> loopRejectedProfiles = Lists.newArrayList();


                File groupFile = new File(String.format(filePathPattern, groupIndex));
                File groupGeoFile = new File(String.format(filePathPattern, groupIndex) + ".geojson");

                // Find similar profiles
                profiles = filterSimilarProfiles(loopInput, loopRejectedProfiles, dictionary, detectionThreshold, thresholdStep);

                // Compute a unique hash, for the group
                int hash = computeProfileHashCode(profiles);
                if (!profileCountByHash.containsKey(hash) && profiles.size() > 0) {

                    if (profiles.size() > minGroupSize) {
                        // Dump to files
                        dumpToGeoJsonFile(profiles, groupGeoFile, true);
                        dumpToFile(profiles, groupFile, true);

                        // Add to hash map
                        profileCountByHash.put(hash, profiles.size());
                        fileByHash.put(hash, groupFile);
                        groupIndex++;
                    }

                    // Will loop on rejected profiles
                    int rejectedHash = computeProfileHashCode(loopRejectedProfiles);
                    if (!profileCountByHash.containsKey(rejectedHash) && loopRejectedProfiles.size() > minGroupSize) {
                        inputs.add(loopRejectedProfiles);
                    }
                }

                detectionThreshold += thresholdStep;

            }
        }

        if (!fileByHash.isEmpty()) {
            log.warn("Found {} groups of similar profiles:", fileByHash.size());
            profileCountByHash.entrySet().forEach(e -> {
                log.warn(" - {} profiles (hash: {}) at {}", e.getValue(), e.getKey(), fileByHash.get(e.getKey()));
            });
        }
    }

    public List<UserProfile> filterSimilarProfiles(List<UserProfile> profiles, List<UserProfile> rejectedProfiles,
                                                   Set<String> dictionary,
                                                   double startDetectionThreshold,
                                                   double thresholdStep) {
        log.info("--- Analysing {} profiles...", profiles.size());

        // Analyse fields
        double detectionThreshold = startDetectionThreshold;
        int counter = 0;
        while (detectionThreshold <= 100) {

            int startSize = profiles.size();

            log.info("-- Pass n°{}: {} profiles - detection threshold: {}%", ++counter, profiles.size(), detectionThreshold);

            // Analyze fields
            profiles = analyseAndFilterOnFields(profiles, rejectedProfiles, dictionary, detectionThreshold, "yyyy-MM", false);

            int delta = startSize - profiles.size();

            if (delta == 0) {
                if (detectionThreshold == startDetectionThreshold) break; // Not need to try again
                // Return to the start threshold
                detectionThreshold = startDetectionThreshold;
            }
            else {
                detectionThreshold += thresholdStep;
            }
        }
        log.info("--- Find {} similar profiles", profiles.size());

        return profiles;
    }

    @Test
    public void findAndDeleteFakeProfiles_543() {
        List<UserProfile> profiles = loadProfiles(UserProfileFilter.builder()
            // Insertion period
            .startDate(Dates.safeParseDate("2021-06-29 23:00", "yyyy-MM-dd HH:mm"))
            .endDate(Dates.safeParseDate("2021-06-30 03:00", "yyyy-MM-dd HH:mm"))
            .build(), UserProfile.Fields.TIME, SortDirection.ASC);

        Assume.assumeNotNull(profiles);
        Assume.assumeTrue(profiles.size() > 0);
        Assume.assumeTrue(profiles.size() >= 540);

        List<UserProfile> rejectedProfiles = Lists.newArrayList(); // Rejected = valid profiles (not fake)

        // Analyse settings presence
        profiles = filterNoSettings(profiles, rejectedProfiles);

        // Analyse movement presence
        profiles = filterNoTx(profiles, rejectedProfiles);

        // Analyse fields
        profiles = analyseAndFilterOnFields(profiles, rejectedProfiles, 90 /*90%*/, false);

        log.info("Find {} fake profiles", profiles.size());
        Assume.assumeTrue("Nothing to delete", profiles.size() > 0);

        // Apply deletion
        deleteProfiles(profiles);
    }


    @Test
    public void findAndDeleteFakeProfiles_4600() {
        List<UserProfile> profiles = loadProfiles(UserProfileFilter.builder()
            // Insertion period
            .startDate(Dates.safeParseDate("2021-06-30 22:00", "yyyy-MM-dd HH:mm"))
            .endDate(Dates.safeParseDate("2021-07-02", "yyyy-MM-dd"))
            .build());

        Assume.assumeNotNull(profiles);
        Assume.assumeTrue(profiles.size() > 0);
        Assume.assumeTrue(profiles.size() >= 4600);

        List<UserProfile> rejectedProfiles = Lists.newArrayList(); // Rejected = valid profiles (not fake)

        // Analyse settings presence
        profiles = filterNoSettings(profiles, rejectedProfiles);

        // Analyse movement presence
        profiles = filterNoTx(profiles, rejectedProfiles);

        // Analyse fields
        profiles = analyseAndFilterOnFields(profiles, rejectedProfiles, 90 /*90%*/, true);

        log.info("Find {} fake profiles", profiles.size());
        Assume.assumeTrue("Nothing to delete", profiles.size() > 0);

        // Apply deletion
        deleteProfiles(profiles);
    }

    @Test
    @Ignore
    public void findAndDeleteFakeProfiles_G() {

        List<UserProfile> profiles = loadProfiles(UserProfileFilter.builder()
            // Should match some profiles (bbox France)
            .boundingBox(Envelope.builder()
                .minX(0).maxX(3)
                .minY(44).maxY(47.9)
                .build())
            // Insertion period
            .startDate(Dates.safeParseDate("2021-07-01", "yyyy-MM-dd"))
            .endDate(Dates.safeParseDate("2021-07-02", "yyyy-MM-dd"))
            .build());

        Assume.assumeNotNull(profiles);
        Assume.assumeTrue(profiles.size() > 0);

        List<UserProfile> rejectedProfiles = Lists.newArrayList();
        List<UserProfile> doubtfulProfiles = Lists.newArrayList();

        // Analyse fields
        profiles = analyseAndFilterOnFields(profiles, rejectedProfiles, 90 /*90%*/, false);

        // Analyse distance between point
        profiles = analyseAndFilterOnDistance(profiles, rejectedProfiles, doubtfulProfiles,true);

        log.info("Find {} fake profiles", profiles.size());
        Assume.assumeTrue("Nothing to delete", profiles.size() > 0);

        // Apply deletion
        //deleteProfiles(profiles);
    }

    @Test
    @Ignore
    public void deleteProfilesFromFile() throws IOException {

        List<UserProfile> profiles = loadProfilesFile(new File("src/test/resources/fake-profiles-G.json"));
        Assert.assertTrue(profiles.size() == 5314);

        List<UserProfile> rejectedProfiles = Lists.newArrayList();

        // Make sure profiles exists
        profiles = filterExists(profiles, rejectedProfiles);

        log.info("Find {} existing profiles", profiles.size());
        Assume.assumeTrue("Nothing to delete", profiles.size() > 0);

        // Apply deletion
        //deleteProfiles(profiles);
    }



    protected List<UserProfile> analyseAndFilterOnFields(List<UserProfile> profiles,
                                                         List<UserProfile> rejectedProfiles,
                                                         double filterThresholdPct,
                                                         boolean outputToFile) {
        return analyseAndFilterOnFields(profiles, rejectedProfiles, null, filterThresholdPct,
            "yyyy-MM-dd", outputToFile);
    }

    protected List<UserProfile> analyseAndFilterOnFields(List<UserProfile> profiles,
                                                         List<UserProfile> rejectedProfiles,
                                                         Set<String> dictionary,
                                                         double filterThresholdPct,
                                                         String timePattern,
                                                         boolean outputToFile) {

        // Analyse avatar
        {
            profiles = analyseAndFilterOnField(profiles, rejectedProfiles, UserProfile.Fields.AVATAR,
                (Attachment avatar) -> avatar != null && StringUtils.isNotBlank(avatar.getContentType())
                    ? Boolean.TRUE : Boolean.FALSE,
                filterThresholdPct
            );
        }

        // Analyse description
        {
            profiles = analyseAndFilterOnField(profiles, rejectedProfiles, UserProfile.Fields.DESCRIPTION,
                (String description) -> StringUtils.isNotBlank(description)
                    ? Boolean.TRUE : Boolean.FALSE,
                filterThresholdPct
            );
        }
        // Analyse address
        {
            // Exists or not
            profiles = analyseAndFilterOnField(profiles, rejectedProfiles, UserProfile.Fields.ADDRESS,
                (String value) -> StringUtils.isNotBlank(value)
                    ? Boolean.TRUE : Boolean.FALSE,
                filterThresholdPct
            );
        }

        // Analyse city
        {
            // Exists or not
            profiles = analyseAndFilterOnField(profiles, rejectedProfiles, UserProfile.Fields.CITY,
                (String city) -> StringUtils.isNotBlank(city)
                    ? Boolean.TRUE : Boolean.FALSE,
                filterThresholdPct
            );

            // With postal code
            profiles = analyseAndFilterOnField(profiles, rejectedProfiles,
                "withPostalCode",
                UserProfile::getCity,
                (String city) -> this.hasPostalCode(city)
                    ? Boolean.TRUE : Boolean.FALSE,
                filterThresholdPct
            );
        }

        // Analyse date
        {
            profiles = analyseAndFilterOnField(profiles, rejectedProfiles,
                UserProfile.Fields.TIME,
                (Long time) -> time == null || time == 0d ? "null" : Dates.formatDate(new Date(time * 1000), timePattern),
                filterThresholdPct
            );
        }

        // Create the dictionary
        if (dictionary == null){
            int wordNbFoundThreshold = 3;
            Map<String, Integer> counterMap = Maps.newHashMap();
            profiles.stream()
                .map(UserProfile::getTitle)
                .flatMap(this::streamWords)
                .forEach(value -> {
                    Integer counter = counterMap.get(value);
                    counterMap.put(value, (counter != null ? counter : 0) + 1);
                });

            dictionary = counterMap.keySet().stream()
                .filter(value -> counterMap.get(value) >= wordNbFoundThreshold)
                .filter(value -> value.length() > 2) // Too short words
                .filter(value -> !value.matches("[A-Z][A-Z]+")) // Capitalize word (family name)
                .collect(Collectors.toSet());
            log.info(" - Word dictionary (appearing at least in {} profiles): {}", wordNbFoundThreshold, dictionary);
        }

        // Analyse title words
        if (dictionary.size() > 0) {
            Set<String> finalDictionary = dictionary;
            profiles = analyseAndFilterOnField(profiles, rejectedProfiles,
                "dictionaryWords",
                UserProfile::getTitle,
                (String title) -> streamWords(title).anyMatch(finalDictionary::contains) ? Boolean.TRUE : Boolean.FALSE,
                filterThresholdPct
            );
        }

        // Analyse geoPoint
        {
            profiles = analyseAndFilterOnField(profiles, rejectedProfiles, UserProfile.Fields.GEO_POINT,
                (GeoPoint point) -> point != null && point.getLon() != null && point.getLat() != null
                    ? Boolean.TRUE : Boolean.FALSE,
                filterThresholdPct
            );
        }

        // Dump to files
        if (outputToFile) {
            log.info("Creating output files...");
            try {
                // Write fake profiles into GeoJson file
                dumpToFile(profiles, new File("target", "fake-profiles-G.json"), true);
                dumpToFile(rejectedProfiles, new File("target", "rejected-profiles.json"), true);
            }
            catch (IOException e) {
                Assume.assumeNoException(e);
            }
        }

        return profiles;
    }

    protected List<UserProfile> analyseAndFilterOnDistance(List<UserProfile> profiles,
                                                           List<UserProfile> rejectedProfiles,
                                                           List<UserProfile> doubtfulProfiles,
                                                           boolean outputToFile) {
        // Parameter for distance rule detection
        double minDistanceMeters = 1750;
        double maxDistanceMeters = 1865;

        final List<UserProfile> profilesCopy = ImmutableList.copyOf(profiles);

        log.error("Analyzing distance between geo points...");
        Map<Long, Integer> counterMap = Maps.newHashMap();
        profiles = profiles.stream().filter((profile) -> {
                double lon = round(profile.getGeoPoint().getLon(), 5);
                double lat = round(profile.getGeoPoint().getLat(), 5);

                long minNeighborDistance = -1;
                int neighborsCounter = 0;
                for (UserProfile other : profilesCopy) {
                    if (!Objects.equals(other.getIssuer(), profile.getIssuer())) {

                        double otherLon = round(other.getGeoPoint().getLon(), 5);
                        double otherLat = round(other.getGeoPoint().getLat(), 5);
                        long distance = Geometries.getDistanceInMeters(lon, lat, otherLon, otherLat);

                        boolean isNeighbor = distance >= minDistanceMeters && distance <= maxDistanceMeters;
                        if (isNeighbor) {
                            neighborsCounter++;
                            if (minNeighborDistance == -1 || distance < minNeighborDistance) {
                                minNeighborDistance = distance;
                            }
                        }
                    }
                }

                if (minNeighborDistance != -1) {
                    // Round at 100 meters
                    minNeighborDistance = Math.round((1d * minNeighborDistance / 100)  + 0.5) * 100;
                    Integer counter = counterMap.get(minNeighborDistance);
                    counterMap.put(minNeighborDistance, (counter != null ? counter : 0) + 1);
                }

                // No neighbor
                if (neighborsCounter == 0) {

                    // Check if settings exists
                    UserSettings settings = settingsService.findByPubkey(peer, profile.getIssuer()).orElse(null);
                    if (settings != null) {
                        rejectedProfiles.add(profile);
                        return false; // Keep it, if settings exists
                    }

                    // Check if movements
                    boolean hasMovements = service.findMovements(peer, MovementFilter.builder()
                                .pubkey(profile.getIssuer()).build(),
                            Page.builder().size(1).build())
                        .anyMatch(Objects::nonNull);
                    if (hasMovements) {
                        rejectedProfiles.add(profile);
                        return false; // Keep it, if movements exists
                    }

                    // Log to understand why there is no neighbor
                /*try {
                    log.debug("Detected doubtful profile: no neighbor BUT no settings and NO movements: {} - {}", objectMapper.writeValueAsString(profile));
                } catch (Exception e) {} // Silent
                doubtfulProfiles.add(profile);
                return false;*/
                }

                return true;
            })
            .collect(Collectors.toList());

        // DEBUG - Log all neightbor distance found
        counterMap.keySet().stream().sorted()
            .forEach(distance -> {
                int counter = counterMap.get(distance);
                //if (counter < 10)
                log.info("distance={} counter={}", distance, counter);
            });

        // Find figure center
        {
            double minLat = profiles.stream()
                .map(profile -> profile.getGeoPoint().getLat())
                .reduce(9999d, Math::min);
            double maxLat = profiles.stream()
                .map(profile -> profile.getGeoPoint().getLat())
                .reduce(-9999d, Math::max);
            double minLon = profiles.stream()
                .map(profile -> profile.getGeoPoint().getLon())
                .reduce(9999d, Math::min);
            double maxLon = profiles.stream()
                .map(profile -> profile.getGeoPoint().getLon())
                .reduce(-9999d, Math::max);

            double centerLat = minLat + (maxLat - minLat) / 2;
            double centerLon = minLon + (maxLon - minLon) / 2;

            // https://www.google.com/maps/@45.93681064446829,1.8011600876707416,16z
            log.info("Figure center: https://www.google.com/maps/@{},{},16z", centerLat, centerLon);
        }

        // Dump to files
        if (outputToFile) {
            log.info("Creating output files...");
            try {
                // Write fake profiles into GeoJson file
                dumpToGeoJsonFile(profiles, new File("target", "fake-profiles-G.geojson"), true);
                dumpToFile(profiles, new File("target", "fake-profiles-G.json"), true);
                dumpToGeoJsonFile(doubtfulProfiles, new File("target", "doubtful-profiles.geojson"), true);
                dumpToGeoJsonFile(rejectedProfiles, new File("target", "rejected-profiles.geojson"), true);
            }
            catch (IOException e) {
                Assume.assumeNoException(e);
            }
        }

        return profiles;
    }

    protected List<UserProfile> filterExists(@NonNull List<UserProfile> profiles, @NonNull List<UserProfile> rejectedProfiles) {
        // Make sure profiles exists
        Map<String, UserProfile> profileByPubkeys = Beans.splitByProperty(profiles, UserProfile.Fields.ISSUER);
        String[] pubkeys = profileByPubkeys.keySet().toArray(new String[0]);
        int size = 1000;

        List<UserProfile> existingProfiles = Lists.newArrayList();

        for (int start = 0; start< pubkeys.length; start+= size) {
            int end = Math.min(start+size, pubkeys.length);
            service.findAllByFilter(peer, UserProfileFilter.builder()
                .issuers(Arrays.copyOfRange(pubkeys, start, end))
                .fields(new String[]{UserProfile.Fields.ISSUER})
                .build(), null)
                .map(UserProfile::getIssuer)
                .map(profileByPubkeys::remove)
                .filter(Objects::nonNull)
                .forEach(existingProfiles::add);
        }

        if (!profileByPubkeys.isEmpty()) {
            rejectedProfiles.addAll(profileByPubkeys.values());
        }

        return existingProfiles;
    }

    protected List<UserProfile> filterNoSettings(@NonNull List<UserProfile> profiles, @NonNull List<UserProfile> rejectedProfiles) {

        log.info("Filtering on 'without settings'...");
        Map<String, UserProfile> profilesByPubkeys = Beans.splitByProperty(profiles, UserProfile.Fields.ISSUER);

        String[] pubkeys = profilesByPubkeys.keySet().toArray(new String[0]);
        int fetchSize = 100;
        for (int i = 0; i < pubkeys.length; i += fetchSize) {

            //log.debug("Check user profile's settings' {}/{}...", i, pubkeys.length);

            int end = Math.min(pubkeys.length, i+fetchSize);
            UserSettingsFilter filter = UserSettingsFilter.builder()
                .issuers(Arrays.copyOfRange(pubkeys, i, end))
                .fields(new String[]{UserSettings.Fields.ISSUER, UserSettings.Fields.TIME})
                .build();

            settingsService.findAllByFilter(peer, filter, null)
                .map(UserSettings::getIssuer)
                // Remove from the result map
                .map(profilesByPubkeys::remove)
                .filter(Objects::nonNull)
                // Add removed profile to the reject list
                .forEach(rejectedProfiles::add);
        }

        List<UserProfile> profilesNoSettings = Lists.newArrayList(profilesByPubkeys.values());
        int delta = profiles.size() - profilesNoSettings.size();
        log.info("Found {} profiles without settings ({} has settings}", profilesNoSettings.size(), delta);

        return profilesNoSettings;
    }

    protected List<UserProfile> filterNoTx(@NonNull List<UserProfile> profiles, @NonNull List<UserProfile> rejectedProfiles) {

        log.info("Filtering on 'without transaction'...");
        Map<String, UserProfile> profilesByPubkeys = Beans.splitByProperty(profiles, UserProfile.Fields.ISSUER);

        String[] pubkeys = profilesByPubkeys.keySet().toArray(new String[0]);
        int fetchSize = 100;
        for (int i = 0; i < pubkeys.length; i += fetchSize) {

            //log.debug("Check user profile's settings' {}/{}...", i, pubkeys.length);

            int end = Math.min(pubkeys.length, i+fetchSize);

            // Get sent movements (where profile is the issuer)
            MovementFilter filter = MovementFilter.builder()
                .issuers(Arrays.copyOfRange(pubkeys, i, end))
                .fields(new String[]{Movement.Fields.ISSUER, Movement.Fields.MEDIAN_TIME})
                .build();
            service.findMovements(peer, filter, null)
                .map(Movement::getIssuer)
                // Remove from the result map
                .map(profilesByPubkeys::remove)
                .filter(Objects::nonNull)
                // Add removed profile to the reject list
                .forEach(rejectedProfiles::add);

            // Get received movements (where profile is the recipient)
            filter = MovementFilter.builder()
                .recipients(Arrays.copyOfRange(pubkeys, i, end))
                .fields(new String[]{Movement.Fields.ISSUER, Movement.Fields.MEDIAN_TIME})
                .build();
            service.findMovements(peer, filter, null)
                .map(Movement::getIssuer)
                // Remove from the result map
                .map(profilesByPubkeys::remove)
                .filter(Objects::nonNull)
                // Add removed profile to the reject list
                .forEach(rejectedProfiles::add);
        }

        List<UserProfile> profilesNoTx = Lists.newArrayList(profilesByPubkeys.values());
        int delta = profiles.size() - profilesNoTx.size();
        log.info("Found {} profiles without Tx ({} has Tx}", profilesNoTx.size(), delta);

        return profilesNoTx;
    }

    protected void deleteProfiles(List<UserProfile> profiles) {
        if (CollectionUtils.isEmpty(profiles)) return; // Skip

        long now = System.currentTimeMillis();
        log.error("Deleting {} fake profiles...", profiles.size());
        int counter = 0;
        while(counter < profiles.size()) {
            UserProfile profile = profiles.get(counter);

            try {
                try {
                    service.delete(peer, wallet, profile);
                    counter++;

                    if (counter % 50 == 0) {
                        log.info("Deleted {}/{} ...", counter, profiles.size());
                        Thread.sleep(30000); // Waiting POD to process
                    }
                }
                catch (HttpTimeoutException e) {
                    // Wait 10s, then loop
                    Thread.sleep(30000);
                }
                catch (HttpNotFoundException nfe) {
                    // OK, continue. Document has been deleted, during a timeout...
                    counter++;
                }
            }
            catch (HttpUnauthorizeException e) {
                throw new TechnicalException("Not authorized to delete a profile. Please check you are using a admin wallet. See test config file", e);
            }

            catch (Exception e) {
                log.error("Cannot delete profile {{}}: {}", profile.getIssuer(), e.getMessage());
                throw new TechnicalException("Failed to delete profile " + profile.getIssuer(), e);
            }
        }

        log.info("All profiles deleted, in {}ms", System.currentTimeMillis() - now);
    }


    protected List<UserProfile> loadProfiles(@NonNull UserProfileFilter filter) {
        return loadProfiles(filter, UserProfile.Fields.ISSUER, null);
    }
    protected List<UserProfile> loadProfiles(@NonNull UserProfileFilter filter, String sortBy, SortDirection sortDirection) {

        //Preconditions.checkNotNull(filter.getStartDate());
        //Preconditions.checkNotNull(filter.getEndDate());

        if (ArrayUtils.isEmpty(filter.getFields())) {
            filter.setFields(
            new String[]{UserProfile.Fields.ISSUER,
                UserProfile.Fields.TITLE,
                UserProfile.Fields.DESCRIPTION,
                UserProfile.Fields.TIME,
                UserProfile.Fields.ADDRESS,
                UserProfile.Fields.CITY,
                UserProfile.Fields.AVATAR + "." + Attachment.JsonFields.CONTENT_TYPE,
                UserProfile.Fields.GEO_POINT
            });
        }

        List<UserProfile> profiles = Lists.newArrayList();
        Page page = Page.builder()
            .size(1000)
            .sortBy(sortBy)
            .sortDirection(sortDirection)
            .build();

        int pageNumber = 1;
        try {
            boolean hasMorePage;
            do {
                List<UserProfile> pageResult = service.findAllByFilter(peer, filter, page)
                    .collect(Collectors.toList());
                int resultCount = CollectionUtils.size(pageResult);
                if (resultCount > 0) profiles.addAll(pageResult);

                hasMorePage = resultCount == page.getSize();
                page.setFrom(pageNumber * page.getSize());
                pageNumber++;
            } while (hasMorePage);
        }
        catch (Exception e) {
            log.error("Error while fetching page #{}: {}", pageNumber, e.getMessage());
            // Continue
        }

        log.info("{} profiles loaded, from filter", profiles.size());

        return profiles;
    }

    private double round(double value, int nbDecimals) {
        return ((double)Math.round(value * Math.pow(10, nbDecimals) -0.5)) / Math.pow(10, nbDecimals);
    }

    private <P, V> List<UserProfile> analyseAndFilterOnField(List<UserProfile> profiles,
                                                             List<UserProfile> rejectedProfiles,
                                                             String propertyPath,
                                                             Function<P, V> propertyValueReducer,
                                                             double filterThresholdPct) {
        return analyseAndFilterOnField(profiles, rejectedProfiles,
            propertyPath,
            (p) -> Beans.getProperty(p, propertyPath),
            propertyValueReducer,
            filterThresholdPct);
    }

    private <P, V> List<UserProfile> analyseAndFilterOnField(List<UserProfile> profiles,
                                                             List<UserProfile> rejectedProfiles,
                                                             String ruleName,
                                                             Function<UserProfile, P> propertyGetter,
                                                             Function<P, V> propertyValueReducer,
                                                             double filterThresholdPct) {
        Map<V, Integer> counterMap = Maps.newHashMap();
        profiles.forEach(profile -> {
            P property = propertyGetter.apply(profile);
            V value = propertyValueReducer.apply(property);
            if (value != null) {
                Integer counter = counterMap.get(value);
                counterMap.put(value, (counter != null ? counter : 0) + 1);
            }
        });

        Comparator<Map.Entry<V, Integer>> valueComparator = Comparator.comparing(Map.Entry::getValue);
        Stream<Map.Entry<V, Integer>> stream = counterMap.entrySet().stream();

        // Sorte by nb profiles (desc)
        if (!ruleName.toLowerCase().contains("time")) {
            stream = stream.sorted(valueComparator.reversed());
        }
        else {
            stream = stream.sorted((e1, e2) -> {
                String time1 = (String)e1.getKey();
                String time2 = (String)e2.getKey();
                return time1.compareTo(time2);
            });
        }

        // Display value / nb profiles
        stream
            .map(Map.Entry::getKey)
            .forEach(value -> {
                int counter = counterMap.get(value);
                log.trace("{}={} nbProfiles={}", ruleName, value, counter);
            });

        // Get the major value
        V majorValue = counterMap.entrySet().stream()
            .sorted(valueComparator.reversed())
            .map(Map.Entry::getKey)
            .findFirst().orElse(null);

        if (counterMap.size() == 0) {
            log.debug("No value found for {} [SKIP]", ruleName);
        }
        else if (counterMap.size() == 1) {
            log.debug("All profiles have {}={} [OK]", ruleName, majorValue);
        }
        else {

            // Check of major > threshold
            int count = counterMap.entrySet().stream()
                .sorted(valueComparator)
                .map(Map.Entry::getValue).reduce(0, Integer::sum);
            double valuePct = round(((double)counterMap.get(majorValue)) / count * 100, 2);
            if (valuePct >= filterThresholdPct) {
                log.debug("{}% profiles have {}={} => [FILTERING]", valuePct, ruleName, majorValue);
                return profiles.stream().filter(profile -> {
                    P property = propertyGetter.apply(profile);
                    V value = propertyValueReducer.apply(property);
                    if (!Objects.equals(value, majorValue)) {
                        rejectedProfiles.add(profile);
                        return false;
                    }
                    return true;
                }).collect(Collectors.toList());
            }
            else {
                log.debug("{}% profiles have {}={} [SKIP] (thresholdPct={}%)", valuePct, ruleName, majorValue, filterThresholdPct);
            }
        }

        return profiles;
    }

    protected void dumpToGeoJsonFile(
        @NonNull List<UserProfile> profiles,
        @NonNull File file,
        boolean deleteIfExists) throws IOException {
        ObjectMapper objectMapper = JacksonUtils.getThreadObjectMapper();

        // Write fake profiles into GeoJson file
        if (file.exists()) {
            if (deleteIfExists) FileUtils.delete(file);
            else throw new TechnicalException("File already exists");
        }

        List<UserProfile> geoProfiles = profiles.stream().filter(this::hasGeoPoint).collect(Collectors.toList());

        if (geoProfiles.size() > 0) {
            objectMapper.writeValue(file, service.toGeoJson(geoProfiles));
            log.debug("Write {} Geo profiles at {}", geoProfiles.size(), file.getAbsolutePath());
        }
    }

    protected void dumpToFile(
        @NonNull List<UserProfile> profiles,
        @NonNull File file,
        boolean deleteIfExists) throws IOException {
        ObjectMapper objectMapper = JacksonUtils.getThreadObjectMapper();

        // Write fake profiles into GeoJson file
        if (file.exists()) {
            if (deleteIfExists) FileUtils.delete(file);
            else throw new TechnicalException("File already exists");
        }
        if (profiles.size() > 0) {
            objectMapper.writeValue(file, profiles);
            log.debug("Write {} profiles at {}", profiles.size(), file.getAbsolutePath());
        }
    }

    protected List<UserProfile> loadProfilesFile(@NonNull File jsonFile) throws IOException {
        Assert.assertTrue(String.format("File %s not exists", jsonFile.getAbsolutePath()), jsonFile.exists());
        ObjectMapper objectMapper = JacksonUtils.getThreadObjectMapper();

        ArrayNode jsonArray = objectMapper.readValue(Files.newInputStream(jsonFile.toPath()), ArrayNode.class);
        Assert.assertNotNull(jsonArray);

        final List<UserProfile> result = Lists.newArrayList();
        jsonArray.forEach(node -> {
            UserProfile profile = null;
            try {
                profile = objectMapper.treeToValue(node, UserProfile.class);
            } catch (JsonProcessingException e) {
                log.error("Cannot deserialize a profile: " + node.asText(), e);
            }
            result.add(profile);
        });

        log.info("Loaded {} profiles from file {}", result.size(), jsonFile.getAbsolutePath());

        return result;
    }

    protected Set<String> createDictionary(List<UserProfile> profiles) {
        return createDictionary(profiles, UserProfile::getTitle);
    }

    protected Set<String> createDictionary(List<UserProfile> profiles, Function<UserProfile, String> getter) {
        Set<String> dictionary = profiles.stream()
                .map(getter)
                .flatMap(this::streamWords)
                .filter(word -> word.length() > 2)
                .sorted()
                .collect(Collectors.toCollection(LinkedHashSet::new));

        log.info("Find {} words, from {} profiles", dictionary.size(), profiles.size());
        Assume.assumeTrue(CollectionUtils.isNotEmpty(dictionary));
        return dictionary;
    }

    protected boolean hasGeoPoint(UserProfile p) {
        return p.getGeoPoint() != null && p.getGeoPoint().getLat() != null && p.getGeoPoint().getLon() != null;
    }

    private Pattern postalCodeRegxExp = Pattern.compile("[0-9]{1,5}");

    protected boolean hasPostalCode(String city) {
        return StringUtils.isNotBlank(city) && postalCodeRegxExp.matcher(city).find();
    }

    protected List<UserProfile> filterDictionary(List<UserProfile> profiles,
                                                 Set<String> dictionary) {
        return filterDictionary(profiles, dictionary, UserProfile::getTitle);
    }

    protected List<UserProfile> filterDictionary(List<UserProfile> profiles,
                                                 Set<String> dictionary,
                                                 Function<UserProfile, String> getter) {
        return profiles.stream()
            .filter(p -> streamWords(getter.apply(p))
                .anyMatch(dictionary::contains))
            .collect(Collectors.toList());
    }

    protected Stream<String> streamWords(String phrase) {
        return (phrase == null)
            ? Stream.empty()
            : Arrays.stream(phrase.split("[ \t]+"))
            .map(String::trim)
            .map(String::toLowerCase);
    }

    protected int computeProfileHashCode(List<UserProfile> profiles) {
        return profiles.stream()
            .map(UserProfile::getIssuer)
            .sorted()
            .collect(Collectors.toCollection(LinkedHashSet::new))
            .hashCode();
    }
}
