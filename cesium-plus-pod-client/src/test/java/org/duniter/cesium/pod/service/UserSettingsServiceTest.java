package org.duniter.cesium.pod.service;

/*-
 * #%L
 * Cesium+ pod :: Client API
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.duniter.cesium.pod.TestResource;
import org.duniter.elasticsearch.client.model.filter.UserSettingsFilter;
import org.duniter.elasticsearch.client.service.UserSettingsService;
import org.duniter.elasticsearch.model.user.UserSettings;
import org.junit.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class UserSettingsServiceTest extends AbstractServiceTest<UserSettingsService>{

    @ClassRule
    public static final TestResource resource = TestResource.create();

    public UserSettingsServiceTest(){
        super(UserSettingsService.class);
    }

    @Before
    public void setUp() {
        super.setUp();
        peer = getPeer(resource.getFixtures().getDefaultCurrency());
        wallet = getWallet(resource.getFixtures().getDefaultCurrency(),
            null,
            resource.getFixtures().getUserPublicKey(),
            resource.getFixtures().getUserSecretKey()
            );
    }

    @Test
    public void findAllByFilter() {

        // Should match some profiles (bbox France)
        {
            UserSettingsFilter filter = UserSettingsFilter.builder()
                .issuers(new String[]{
                    wallet.getPubKeyHash()
                })
                .build();

            List<UserSettings> profiles = service.findAllByFilter(peer, filter, null)
                .peek(up -> log.debug(" - issuer: {}", up.getIssuer()))
                .collect(Collectors.toList());

            Assert.assertNotNull(profiles);
            Assert.assertTrue(profiles.size() > 0);
        }

        // Get only some fields
        {
            UserSettingsFilter filter = UserSettingsFilter.builder()
                .issuers(new String[]{
                    wallet.getPubKeyHash()
                })
                .fields(new String[]{UserSettings.Fields.ISSUER, UserSettings.Fields.TIME})
                .build();

            List<UserSettings> profiles = service.findAllByFilter(peer, filter, null)
                .peek(up -> log.debug(" - time: {}", up.getTime()))
                .collect(Collectors.toList());

            Assert.assertNotNull(profiles);
            Assert.assertTrue(profiles.size() > 0);
        }
    }

    @Test
    public void findByPubkey() {

        UserSettings settings = service.findByPubkey(peer, resource.getFixtures().getUserPublicKey()).orElseGet(null);
        Assert.assertNotNull(settings);
        Assert.assertNotNull(settings.getIssuer());
        Assert.assertNotNull(settings.getContent());
        Assert.assertNotNull(settings.getTime());
        Assert.assertNotNull(settings.getNonce());
    }

    @Test
    @Ignore
    public void save() {

        // TODO
        UserSettings source = UserSettings.builder()
            //.content()
            .build();

        UserSettings savedSettings = service.save(peer, wallet, source);
        Assert.assertNotNull(savedSettings);
        Assert.assertEquals(savedSettings.getIssuer(), wallet.getPubKeyHash());
        Assert.assertEquals(savedSettings.getIssuer(), savedSettings.getId());
    }

}
