package org.duniter.cesium.pod.service;

/*-
 * #%L
 * Cesium+ pod :: Client API
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.duniter.cesium.pod.TestResource;
import org.duniter.core.beans.Service;
import org.duniter.core.client.config.Configuration;
import org.duniter.core.client.model.local.Peer;
import org.duniter.core.client.model.local.Wallet;
import org.duniter.core.client.service.HttpService;
import org.duniter.elasticsearch.client.service.ServiceLocator;
import org.duniter.elasticsearch.client.service.UserProfileService;
import org.junit.Assume;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

@Slf4j
public abstract class AbstractServiceTest<S extends Service> {

    private final Class<S> serviceClass;
    protected S service;

    protected Peer peer;
    protected Wallet wallet;

    protected Configuration configuration;

    protected AbstractServiceTest(Class<S> serviceClass) {
        this.serviceClass = serviceClass;
    }

    @Before
    public void setUp() {
        service = ServiceLocator.instance().getBean(serviceClass);
        Assume.assumeNotNull(service);

        configuration = Configuration.instance();
        Assume.assumeNotNull(configuration);
    }

    protected Peer getPeer(String currency) {
        return ServiceLocator.instance().getPeerService().getActivePeerByCurrency(currency);
    }

    protected Wallet getWallet(String currency, String uid, String pubkey, String seckey) {
        return new Wallet(currency, uid, pubkey, seckey);
    }
}
