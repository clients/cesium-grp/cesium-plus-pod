package org.duniter.elasticsearch.client.model.query;

/*-
 * #%L
 * Cesium+ pod :: Client API
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.duniter.elasticsearch.model.type.GeoPoint;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeoBoundingBoxQuery {

    private BoundingBox geoPoint;

    @Data
    @Builder
    @AllArgsConstructor
    public static class BoundingBox {

        private GeoPoint topLeft;
        private GeoPoint bottomRight;

        @JsonGetter("top_left")
        public GeoPoint getTopLeft() {
            return topLeft;
        }

        @JsonGetter("bottom_right")
        public GeoPoint getBottomRight() {
            return bottomRight;
        }
    }

}
