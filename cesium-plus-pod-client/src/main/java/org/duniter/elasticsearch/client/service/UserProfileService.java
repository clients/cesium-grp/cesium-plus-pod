package org.duniter.elasticsearch.client.service;

/*-
 * #%L
 * Cesium+ pod :: Client API
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.duniter.core.beans.Service;
import org.duniter.core.client.model.local.Peer;
import org.duniter.core.client.model.local.Wallet;
import org.duniter.elasticsearch.model.Page;
import org.duniter.elasticsearch.client.model.filter.MovementFilter;
import org.duniter.elasticsearch.client.model.filter.UserProfileFilter;
import org.duniter.elasticsearch.model.blockchain.Movement;
import org.duniter.elasticsearch.model.user.UserProfile;

import javax.annotation.Nullable;
import java.util.stream.Stream;

import org.geojson.FeatureCollection;

public interface UserProfileService extends Service {

    Stream<UserProfile> findAllByFilter(Peer peer, UserProfileFilter filter, @Nullable Page page);

    UserProfile save(Peer peer, Wallet wallet, UserProfile userProfile);

    UserProfile update(Peer peer, Wallet wallet, UserProfile userProfile);

    FeatureCollection toGeoJson(Iterable<UserProfile> profiles, String... fields);

    Stream<Movement> findMovements(Peer peer, MovementFilter filter, @Nullable Page page);

    boolean deleteByPubkey(Peer peer, Wallet wallet, String pubkey);

    boolean delete(Peer peer, Wallet wallet, UserProfile userProfile);
}
