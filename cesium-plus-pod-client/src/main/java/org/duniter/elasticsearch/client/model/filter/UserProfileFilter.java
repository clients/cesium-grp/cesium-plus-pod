package org.duniter.elasticsearch.client.model.filter;

/*-
 * #%L
 * Cesium+ pod :: Client API
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.duniter.elasticsearch.client.model.geom.Envelope;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserProfileFilter {

    public static UserProfileFilter nullToEmpty(UserProfileFilter filter) {
        return filter != null ? filter : UserProfileFilter.builder().build();
    }

    @Builder.Default
    private Date startDate = null;

    @Builder.Default
    private Date endDate = null;

    @Builder.Default
    private Envelope boundingBox = null;

    @Builder.Default
    private String[] fields = null;

    @Builder.Default
    private String queryString = null;

    @Builder.Default
    private String[] issuers = null;
}
