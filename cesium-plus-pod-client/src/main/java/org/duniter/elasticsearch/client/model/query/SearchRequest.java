package org.duniter.elasticsearch.client.model.query;

/*-
 * #%L
 * Cesium+ pod :: Client API
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.collect.ImmutableMap;
import lombok.Builder;
import lombok.Data;
import org.duniter.core.util.StringUtils;
import org.duniter.elasticsearch.model.SortDirection;

import java.util.Map;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchRequest {

    public static class SearchRequestBuilder{
        public SearchRequestBuilder queryString(String queryString) {
            this.query(SearchQuery.builder()
                    .queryString(QueryString.builder()
                        .query(queryString)
                        .build())
                .build());
            return SearchRequestBuilder.this;
        }

        public SearchRequestBuilder sortBy(String field, SortDirection direction) {
            if (StringUtils.isNotBlank(field)) {
                this.sort(ImmutableMap.of(field, direction == null || direction == SortDirection.ASC ? "asc" : "desc"));
            }
            return SearchRequestBuilder.this;
        }
    }

    private SearchQuery query;

    Integer from;
    Integer size;

    String[] source;

    Map<String, String> sort;

    @JsonGetter("_source")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String[] getSource() {
        return source;
    }
}
