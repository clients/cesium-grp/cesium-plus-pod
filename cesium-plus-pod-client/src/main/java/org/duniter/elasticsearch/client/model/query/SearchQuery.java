package org.duniter.elasticsearch.client.model.query;

/*-
 * #%L
 * Cesium+ pod :: Client API
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchQuery {

    private GeoBoundingBoxQuery geoBoundingBox;
    private QueryString queryString;

    private ExistsQuery exists;

    private BoolQuery bool;

    private ConstantScoreQuery constantScore;

    private Map<String, String> match;
    private Map<String, String> matchPhrase;
    private Map<String, String> matchPhrasePrefix;

    private Map<String, String> prefix;

    private Map<String, String> term;
    private Map<String, String[]> terms;

    private Map<String, RangePartQuery> range;

    @JsonGetter("constant_score")
    public ConstantScoreQuery getConstantScore() {
        return constantScore;
    }

    @JsonGetter("query_string")
    public QueryString getQueryString() {
        return queryString;
    }

    @JsonGetter("geo_bounding_box")
    public GeoBoundingBoxQuery getGeoBoundingBox() {
        return geoBoundingBox;
    }

    @JsonGetter("match_phrase")
    public Map<String, String> getMatchPhrase() {
        return matchPhrase;
    }
    @JsonGetter("match_phrase_prefix")
    public Map<String, String> getMatchPhrasePrefix() {
        return matchPhrasePrefix;
    }

}
