package org.duniter.elasticsearch.client.service;

/*-
 * #%L
 * Cesium+ pod :: Client API
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.duniter.core.client.config.Configuration;
import org.duniter.core.client.model.local.Peer;
import org.duniter.core.client.model.local.Wallet;
import org.duniter.core.client.service.exception.HttpNotFoundException;
import org.duniter.core.exception.TechnicalException;
import org.duniter.core.util.ArrayUtils;
import org.duniter.core.util.Preconditions;
import org.duniter.core.util.StringUtils;
import org.duniter.elasticsearch.model.Page;
import org.duniter.elasticsearch.client.model.filter.UserSettingsFilter;
import org.duniter.elasticsearch.client.model.query.*;
import org.duniter.elasticsearch.dao.user.IUserSettingsRepository;
import org.duniter.elasticsearch.model.DeleteRecord;
import org.duniter.elasticsearch.model.query.SearchResponse;
import org.duniter.elasticsearch.model.user.UserSettings;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@Slf4j
public class UserSettingsServiceImpl extends AbstractServiceImpl implements UserSettingsService {

    protected Configuration config;

    public interface Uris {
        String SEARCH = "/user/settings/_search";

        String GET_SOURCE_BY_PUBKEY = "/user/settings/%s/_source";

        String POST_INSERT = "/user/settings";

        String POST_DELETE = "/history/delete";

    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        config = Configuration.instance();
    }

    @Override
    public void close() throws IOException {
        super.close();
    }

    @Override
    public Stream<UserSettings> findAllByFilter(@NonNull Peer peer,
                                                @NonNull UserSettingsFilter filter,
                                                @Nullable Page page) {
        page = Page.nullToDefault(page);

        ObjectMapper objectMapper = getObjectMapper();
        try {

            List<SearchQuery> filters = Lists.newArrayList();
            List<SearchQuery> matches = Lists.newArrayList();

            if (ArrayUtils.isNotEmpty(filter.getIssuers())) {
                filters.add(SearchQueries
                    .terms(UserSettings.Fields.ISSUER, filter.getIssuers())
                );
            }

            // Query String
            if (StringUtils.isNotBlank(filter.getQueryString())) {
                matches.add(SearchQueries.queryString(filter.getQueryString()));
            }

            SearchQuery query = SearchQuery.builder()
                .bool(BoolQuery.builder()
                    .must(matches.toArray(new SearchQuery[0]))
                    .filter(filters.toArray(new SearchQuery[0]))
                    .build())
                .build();

            SearchRequest request = SearchRequest.builder()
                .query(query)
                .from(page.getFrom())
                .size(page.getSize())
                .source(filter.getFields())
                .sortBy(page.getSortBy(), page.getSortDirection())
                .build();

            long now = System.currentTimeMillis();
            if (log.isDebugEnabled()) {
                log.debug("Searching on user profiles using: {}", objectMapper.writeValueAsString(request));
            }

            HttpPost httpPost = new HttpPost(httpService.getPath(peer, Uris.SEARCH));
            HttpEntity entity = EntityBuilder.create()
                .setContentType(ContentType.APPLICATION_JSON)
                .setBinary(objectMapper.writeValueAsBytes(request))
                .build();
            httpPost.setEntity(entity);
            SearchResponse response = httpService.executeRequest(httpPost, SearchResponse.class);

            if (log.isDebugEnabled()) {
                log.debug("Searching on user settings [OK] - {} result(s) found in {}ms",
                    ArrayUtils.size(response.getHits().getHits()),
                    System.currentTimeMillis() - now);
            }

            return Arrays.stream(response.getHits().getHits())
                .map(hit -> {
                    try {
                        return objectMapper.treeToValue(hit.getSource(), UserSettings.class);
                    } catch (JsonProcessingException e) {
                        log.warn("Error during user settings deserialization");
                        return null;
                    }
                })
                .filter(Objects::nonNull);

        } catch (JsonProcessingException e) {
            throw new TechnicalException(e);
        }
    }

    @Override
    public UserSettings save(Peer peer, Wallet wallet, UserSettings userSettings) {
        fillDefaults(wallet, userSettings);
        return sendRecord(peer, Uris.POST_INSERT, wallet, userSettings);
    }

    @Override
    public Optional<UserSettings> findByPubkey(Peer peer, String pubkey) {

        try {
            UserSettings settings = httpService.executeRequest(peer,
                String.format(Uris.GET_SOURCE_BY_PUBKEY, pubkey),
                UserSettings.class);
            return Optional.of(settings);
        }
        catch (HttpNotFoundException e) {
            return Optional.empty(); // Not found
        }
    }

    @Override
    public boolean delete(@NonNull Peer peer, @NonNull Wallet wallet, @NonNull UserSettings userSettings) {
        Preconditions.checkNotNull(userSettings.getIssuer());
        return deleteByPubkey(peer, wallet, userSettings.getIssuer());
    }

    @Override
    public boolean deleteByPubkey(Peer peer, Wallet wallet, String pubkey) {
        DeleteRecord record = new DeleteRecord();
        record.setIndex(IUserSettingsRepository.INDEX);
        record.setType(IUserSettingsRepository.TYPE);
        record.setId(pubkey);

        DeleteRecord savedRecord = sendRecord(peer, Uris.POST_DELETE, wallet, record);

        return StringUtils.isNotBlank(savedRecord.getId());
    }

}
