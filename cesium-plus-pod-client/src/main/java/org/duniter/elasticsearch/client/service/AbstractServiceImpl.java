package org.duniter.elasticsearch.client.service;

/*-
 * #%L
 * Cesium+ pod :: Client API
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.duniter.core.client.config.Configuration;
import org.duniter.core.client.model.bma.jackson.JacksonUtils;
import org.duniter.core.client.model.local.Peer;
import org.duniter.core.client.model.local.Wallet;
import org.duniter.core.client.service.bma.BaseRemoteServiceImpl;
import org.duniter.core.exception.TechnicalException;
import org.duniter.core.service.CryptoService;
import org.duniter.core.util.json.JsonAttributeParser;
import org.duniter.elasticsearch.model.Record;
import org.duniter.elasticsearch.model.Records;
import org.duniter.elasticsearch.model.user.UserEvent;
import org.duniter.elasticsearch.model.user.UserProfile;

import java.io.IOException;

@Slf4j
public abstract class AbstractServiceImpl extends BaseRemoteServiceImpl {

    protected static JsonAttributeParser<String> PARSER_HASH = new JsonAttributeParser<>(Record.Fields.HASH, String.class);
    protected static JsonAttributeParser<String> PARSER_SIGNATURE = new JsonAttributeParser<>(Record.Fields.SIGNATURE, String.class);

    protected Configuration config;
    protected CryptoService cryptoService;

    protected ObjectMapper objectMapper;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        config = Configuration.instance();
        cryptoService = ServiceLocator.instance().getCryptoService();
    }

    @Override
    public void close() throws IOException {
        super.close();
    }

    public <T extends Record> T sendRecord(Peer peer, String path, Wallet wallet, T record) {
        ObjectMapper objectMapper = getObjectMapper();
        fillDefaults(wallet, record);

        // Add hash + signature
        T signedRecord = addHashAndSignature(wallet, record);


        try {

            if (log.isDebugEnabled()) {
                String json = objectMapper.writeValueAsString(signedRecord);
                log.debug("Sending POST request to [{}]: {}", path, json);
            }

            HttpPost httpPost = new HttpPost(httpService.getPath(peer, path));
            HttpEntity entity = EntityBuilder.create()
                .setContentType(ContentType.APPLICATION_JSON)
                .setBinary(objectMapper.writeValueAsBytes(signedRecord))
                .build();
            httpPost.setEntity(entity);

            // Send it to pod
            String id = this.httpService.executeRequest(httpPost, String.class);

            // Save the id
            signedRecord.setId(id);

            return signedRecord;

        } catch(JsonProcessingException e) {
            throw new TechnicalException("Unable to serialize UserEvent object", e);
        }
    }

    protected <T extends Record> T addHashAndSignature(@NonNull Wallet wallet, @NonNull T record) {

        String hash = cryptoService.hash(toJson(record, true));
        record.setHash(hash);

        String signature = cryptoService.sign(hash, wallet.getSecKey());
        record.setSignature(signature);

        return record;
    }

    private <T extends Record> String toJson(T record, boolean cleanHashAndSignature) {
        try {
            String json = getObjectMapper().writeValueAsString(record);
            if (cleanHashAndSignature) {
                json = PARSER_SIGNATURE.removeFromJson(json);
                json = PARSER_HASH.removeFromJson(json);
            }
            return json;
        } catch(JsonProcessingException e) {
            throw new TechnicalException("Unable to serialize UserEvent object", e);
        }
    }

    protected <R extends Record> void fillDefaults(Wallet wallet, R record) {

        record.setIssuer(wallet.getPubKeyHash());
        // Set time
        if (record.getTime() == null) {
            record.setTime(System.currentTimeMillis() / 1000);
        }
        // Set version
        if (record.getVersion() == null) {
            record.setVersion(Records.PROTOCOL_VERSION);
        }
    }

    protected ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            objectMapper = JacksonUtils.getThreadObjectMapper();
        }
        return objectMapper;
    }
}
