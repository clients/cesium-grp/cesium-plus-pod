package org.duniter.elasticsearch.client.service;

/*-
 * #%L
 * Cesium+ pod :: Client API
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.duniter.core.beans.Bean;

import java.io.Closeable;
import java.io.IOException;

public class ServiceLocator extends org.duniter.core.client.service.ServiceLocator implements Closeable {
    /**
     * The shared instance of this ServiceLocator.
     */
    private static ServiceLocator instance = new ServiceLocator();

    /**
     * Gets the shared instance of this Class
     *
     * @return the shared service locator instance.
     */
    public static ServiceLocator instance() {
        return instance;
    }


    org.duniter.core.client.service.ServiceLocator delegate;

    protected ServiceLocator() {
        init();
    }

    public void init() {
        if (delegate == null) {
            delegate = org.duniter.core.client.service.ServiceLocator.instance();
            delegate.init();
        }
    }

    @Override
    public void close() throws IOException {
        if (delegate != null)
            delegate.close();
    }

    public UserProfileService getUserProfileService() {
        return delegate.getBean(UserProfileService.class);
    }

    public UserSettingsService getUserSettingsService() {
        return delegate.getBean(UserSettingsService.class);
    }

    public <S extends Bean> S getBean(Class<S> clazz) {
        if (delegate == null) init();
        return delegate.getBean(clazz);
    }
}
