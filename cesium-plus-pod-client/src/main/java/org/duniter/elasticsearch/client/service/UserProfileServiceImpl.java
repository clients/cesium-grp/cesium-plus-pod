package org.duniter.elasticsearch.client.service;

/*-
 * #%L
 * Cesium+ pod :: Client API
 * %%
 * Copyright (C) 2014 - 2023 Duniter Team
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Lists;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.duniter.core.client.config.Configuration;
import org.duniter.core.client.model.local.Peer;
import org.duniter.core.client.model.local.Wallet;
import org.duniter.core.exception.TechnicalException;
import org.duniter.core.util.ArrayUtils;
import org.duniter.core.util.Beans;
import org.duniter.core.util.Preconditions;
import org.duniter.core.util.StringUtils;
import org.duniter.elasticsearch.model.Page;
import org.duniter.elasticsearch.client.model.filter.MovementFilter;
import org.duniter.elasticsearch.client.model.filter.UserProfileFilter;
import org.duniter.elasticsearch.client.model.query.*;
import org.duniter.elasticsearch.dao.user.IUserProfileRepository;
import org.duniter.elasticsearch.model.DeleteRecord;
import org.duniter.elasticsearch.model.blockchain.Movement;
import org.duniter.elasticsearch.model.query.SearchResponse;
import org.duniter.elasticsearch.model.type.Attachment;
import org.duniter.elasticsearch.model.user.UserProfile;
import org.duniter.elasticsearch.model.user.UserSettings;
import org.duniter.elasticsearch.utils.Dates;
import org.duniter.elasticsearch.utils.Geometries;
import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.geojson.GeoJsonObject;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class UserProfileServiceImpl extends AbstractServiceImpl implements UserProfileService {

    protected Configuration config;

    public interface Uris {
        String SEARCH = "/user/profile/_search";

        String POST_INSERT = "/user/profile";
        String POST_UPDATE = "/user/profile/%s/_update";

        String POST_DELETE = "/history/delete";

        String SEARCH_MOVEMENTS = "/%s/movement/_search";
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        config = Configuration.instance();
    }

    @Override
    public void close() throws IOException {
        super.close();
    }

    @Override
    public Stream<UserProfile> findAllByFilter(@NonNull Peer peer,
                                               @NonNull UserProfileFilter filter,
                                               @Nullable Page page) {
        page = Page.nullToDefault(page);

        ObjectMapper objectMapper = getObjectMapper();
        try {

            List<SearchQuery> matches = Lists.newArrayList();
            List<SearchQuery> filters = Lists.newArrayList();

            // Issuers
            if (ArrayUtils.isNotEmpty(filter.getIssuers())) {
                filters.add(SearchQueries
                    .terms(UserSettings.Fields.ISSUER, filter.getIssuers())
                );
            }

            // Query String
            if (StringUtils.isNotBlank(filter.getQueryString())) {
                matches.add(SearchQueries.queryString(filter.getQueryString()));
            }

            if (filter.getBoundingBox() != null) {
                filters.add(SearchQueries.exists(UserProfile.Fields.GEO_POINT));
                filters.add(SearchQueries.geoBoundingBox(filter.getBoundingBox()));
            }
            if (filter.getStartDate() != null && filter.getEndDate() != null)  {
                matches.add(SearchQueries.range(UserProfile.Fields.TIME, RangePartQuery.builder()
                    .gte(Dates.toUnixTimestamp(filter.getStartDate()))
                    .lt(Dates.toUnixTimestamp(filter.getEndDate()))
                    .build()));
            }
            else if (filter.getStartDate() != null) {
                matches.add(SearchQueries.range(UserProfile.Fields.TIME, RangePartQuery.builder()
                        .gte(Dates.toUnixTimestamp(filter.getStartDate()))
                    .build()));
            }
            else if (filter.getEndDate() != null) {
                matches.add(SearchQueries.range(UserProfile.Fields.TIME, RangePartQuery.builder()
                    .lte(Dates.toUnixTimestamp(filter.getEndDate()))
                    .build()));
            }

            SearchQuery query = SearchQuery.builder()
                .bool(BoolQuery.builder()
                    .must(matches.toArray(new SearchQuery[0]))
                    .filter(filters.toArray(new SearchQuery[0]))
                    .build())
                .build();


            SearchRequest request = SearchRequest.builder()
                .query(query)
                .from(page.getFrom())
                .size(page.getSize())
                .source(filter.getFields())
                .sortBy(page.getSortBy(), page.getSortDirection())
                .build();

            long now = System.currentTimeMillis();
            if (log.isDebugEnabled()) {
                log.debug("Searching on user profiles using: {}", objectMapper.writeValueAsString(request));
            }

            HttpPost httpPost = new HttpPost(httpService.getPath(peer, Uris.SEARCH));
            HttpEntity entity = EntityBuilder.create()
                .setContentType(ContentType.APPLICATION_JSON)
                .setBinary(objectMapper.writeValueAsBytes(request))
                .build();
            httpPost.setEntity(entity);
            SearchResponse response = httpService.executeRequest(httpPost, SearchResponse.class);

            if (log.isDebugEnabled()) {
                log.debug("Searching on user profiles [OK] - {} result(s) found (total: {}) in {}ms",
                    ArrayUtils.size(response.getHits().getHits()),
                    response.getHits().getTotal(),
                    System.currentTimeMillis() - now);
            }

            return Arrays.stream(response.getHits().getHits())
                .map(hit -> {
                    try {
                        return objectMapper.treeToValue(hit.getSource(), UserProfile.class);
                    } catch (JsonProcessingException e) {
                        log.warn("Error during user profile deserialization");
                        return null;
                    }
                })
                .filter(Objects::nonNull);

        } catch (JsonProcessingException e) {
            throw new TechnicalException(e);
        }

    }

    @Override
    public UserProfile save(Peer peer, Wallet wallet, UserProfile userProfile) {
        fillDefaults(wallet, userProfile);
        return sendRecord(peer, Uris.POST_INSERT, wallet, userProfile);
    }

    @Override
    public UserProfile update(Peer peer, Wallet wallet, @NonNull UserProfile userProfile) {
        fillDefaults(wallet, userProfile);
        return sendRecord(peer, String.format(Uris.POST_UPDATE, userProfile.getId()), wallet, userProfile);
    }


    @Override
    public Stream<Movement> findMovements(@NonNull Peer peer, @NonNull MovementFilter filter, @Nullable Page page) {
        Preconditions.checkNotNull(peer.getCurrency());

        page = Page.nullToDefault(page);

        ObjectMapper objectMapper = getObjectMapper();
        try {

            List<SearchQuery> matches = Lists.newArrayList();
            List<SearchQuery> filters = Lists.newArrayList();
            // Issuers
            if (ArrayUtils.isNotEmpty(filter.getIssuers())) {
                filters.add(SearchQueries.terms(Movement.Fields.ISSUER, filter.getIssuers()));
            }
            // Recipients
            if (ArrayUtils.isNotEmpty(filter.getRecipients())) {
                filters.add(SearchQueries.terms(Movement.Fields.RECIPIENT, filter.getRecipients()));
            }
            // Pubkey (=issuer or recipient)
            if (StringUtils.isNotBlank(filter.getPubkey())) {
                matches.add(SearchQueries.queryString(String.format("issuer:%s OR recipient:%s", filter.getPubkey(), filter.getPubkey())));
            }
            if (filter.getStartDate() != null && filter.getEndDate() != null)  {
                matches.add(SearchQueries.range(Movement.Fields.MEDIAN_TIME, RangePartQuery.builder()
                    .gte(Dates.toUnixTimestamp(filter.getStartDate()))
                    .lt(Dates.toUnixTimestamp(filter.getEndDate()))
                    .build()));
            }
            else if (filter.getStartDate() != null) {
                matches.add(SearchQueries.range(Movement.Fields.MEDIAN_TIME, RangePartQuery.builder()
                    .gte(Dates.toUnixTimestamp(filter.getStartDate()))
                    .build()));
            }
            else if (filter.getEndDate() != null) {
                matches.add(SearchQueries.range(Movement.Fields.MEDIAN_TIME, RangePartQuery.builder()
                    .lte(Dates.toUnixTimestamp(filter.getEndDate()))
                    .build()));
            }

            SearchQuery query = SearchQuery.builder()
                .bool(BoolQuery.builder()
                    .must(matches.toArray(new SearchQuery[0]))
                    .filter(filters.toArray(new SearchQuery[0]))
                    .build())
                .build();

            SearchRequest request = SearchRequest.builder()
                .query(query)
                .from(page.getFrom())
                .size(page.getSize())
                .source(filter.getFields())
                .build();

            long now = System.currentTimeMillis();
            if (log.isDebugEnabled()) {
                log.debug("Searching on movements using: {}", objectMapper.writeValueAsString(request));
            }

            HttpPost httpPost = new HttpPost(httpService.getPath(peer, String.format(Uris.SEARCH_MOVEMENTS, peer.getCurrency())));
            HttpEntity entity = EntityBuilder.create()
                .setContentType(ContentType.APPLICATION_JSON)
                .setBinary(objectMapper.writeValueAsBytes(request))
                .build();
            httpPost.setEntity(entity);
            SearchResponse response = httpService.executeRequest(httpPost, SearchResponse.class);

            if (log.isDebugEnabled()) {
                log.debug("Searching on movements [OK] - {} result(s) found in {}ms",
                    ArrayUtils.size(response.getHits().getHits()),
                    System.currentTimeMillis() - now);
            }

            return Arrays.stream(response.getHits().getHits())
                .map(hit -> {
                    try {
                        return objectMapper.treeToValue(hit.getSource(), Movement.class);
                    } catch (JsonProcessingException e) {
                        log.warn("Error during movement deserialization");
                        return null;
                    }
                })
                .filter(Objects::nonNull);

        } catch (JsonProcessingException e) {
            throw new TechnicalException(e);
        }
    }

    @Override
    public boolean delete(@NonNull Peer peer, @NonNull Wallet wallet, @NonNull UserProfile userProfile) {
        Preconditions.checkNotNull(userProfile.getIssuer());
        return deleteByPubkey(peer, wallet, userProfile.getIssuer());
    }

    @Override
    public boolean deleteByPubkey(Peer peer, Wallet wallet, String pubkey) {
        DeleteRecord record = new DeleteRecord();
        record.setIndex(IUserProfileRepository.INDEX);
        record.setType(IUserProfileRepository.TYPE);
        record.setId(pubkey);

        DeleteRecord savedRecord = sendRecord(peer, Uris.POST_DELETE, wallet, record);

        return StringUtils.isNotBlank(savedRecord.getId());
    }

    @Override
    public FeatureCollection toGeoJson(Iterable<UserProfile> profiles, String... fields) {
        FeatureCollection features = new FeatureCollection();

        Set<String> fieldList = ArrayUtils.isNotEmpty(fields)
            ? ImmutableSortedSet.copyOf(fields)
            : ImmutableSortedSet.of(UserProfile.Fields.ISSUER,
                UserProfile.Fields.TITLE,
                UserProfile.Fields.DESCRIPTION,
                UserProfile.Fields.ADDRESS,
                UserProfile.Fields.CITY);

        List<Feature> rows = Beans.getStream(profiles)
            .map(profile -> {
                Feature feature = new Feature();

                // Set properties
                feature.setProperties(fieldList.stream()
                    .map(property -> {
                        Object value = Beans.getProperty(profile, property);
                        if (value == null) return null; // Will be filtered
                        return new AbstractMap.SimpleEntry<>(property, value);
                    })
                    .filter(Objects::nonNull) // Avoid null value
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))
                );

                // Set geometry
                GeoJsonObject geometry = Geometries.toGeometry(profile.getGeoPoint());
                feature.setGeometry(geometry);

                return feature;
            })
            .filter(feature -> feature.getGeometry() != null)
            .collect(Collectors.toList());

        features.setFeatures(rows);

        return features;
    }

    protected void fillDefaults(Wallet wallet, UserProfile userProfile) {
        super.fillDefaults(wallet, userProfile);

        // Workaround, to clear avatar - Is it a bug in the ES attachment-mapper ?
        if (userProfile.getAvatar() == null) {
            userProfile.setAvatar(Attachment.builder()
                .content("")
                .contentType("")
                .build());
        }
    }
}
